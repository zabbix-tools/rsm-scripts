#!/bin/bash

set -o errexit

for tool in resolver nameserver rdds43-server web-server; do
	pid_file="/tmp/$tool.pid"

	if [ -f $pid_file ]; then
		pid=$(cat $pid_file)

		if [ -n "$pid" ]; then
			if ps $pid >/dev/null 2>&1; then
				kill $pid
			fi
		fi

		rm $pid_file
	fi
done

[test-case]

"Service gets disabled while active incident, measurement files are created accordingly"

[prepare-server-database]

[set-global-macro]

"{$RSM.MONITORING.TARGET}"      ,"registry"
"{$RSM.IP4.ROOTSERVERS1}"       ,"193.0.14.129,192.5.5.241,199.7.83.42,198.41.0.4,192.112.36.4"
"{$RSM.IP6.ROOTSERVERS1}"       ,"2001:7fe::53,2001:500:2f::f,2001:500:9f::42,2001:503:ba3e::2:30,2001:500:12::d0d"
"{$RSM.DNS.PROBE.ONLINE}"       ,"2"
"{$RSM.IP4.MIN.PROBE.ONLINE}"   ,"2"
"{$RSM.IP6.MIN.PROBE.ONLINE}"   ,"2"
"{$RSM.INCIDENT.DNS.FAIL}"      ,"2"
"{$RSM.INCIDENT.DNS.RECOVER}"   ,"2"
"{$RSM.INCIDENT.DNSSEC.FAIL}"   ,"2"
"{$RSM.INCIDENT.DNSSEC.RECOVER}","2"

[rsm-api]

"/probeNodes/Probe1-Server1","PUT",200,"readwrite","000-input-probe1.json",""
"/probeNodes/Probe2-Server1","PUT",200,"readwrite","000-input-probe2.json",""

"/tlds/tld2","PUT",200,"readwrite","000-input-tld2-dnssec-min.json",""

[fill-history]

"Global macro history","rsm.configvalue[RSM.DNS.DELAY]"              ,60,1611252048,  60,  60,  60,  60
"Global macro history","rsm.configvalue[RSM.DNS.ROLLWEEK.SLA]"       ,60,1611252053, 240, 240, 240, 240
"Global macro history","rsm.configvalue[RSM.DNS.UDP.RTT.HIGH]"       ,60,1611252051,2500,2500,2500,2500
"Global macro history","rsm.configvalue[RSM.DNS.UDP.RTT.LOW]"        ,60,1611252008, 500, 500, 500, 500
"Global macro history","rsm.configvalue[RSM.INCIDENT.DNS.FAIL]"      ,60,1611252040,   3,   3,   3,   3
"Global macro history","rsm.configvalue[RSM.INCIDENT.DNS.RECOVER]"   ,60,1611252041,   3,   3,   3,   3
"Global macro history","rsm.configvalue[RSM.INCIDENT.DNSSEC.FAIL]"   ,60,1611252042,   3,   3,   3,   3
"Global macro history","rsm.configvalue[RSM.INCIDENT.DNSSEC.RECOVER]",60,1611252043,   3,   3,   3,   3
"Global macro history","rsm.configvalue[RSM.SLV.DNS.DOWNTIME]"       ,60,1611252006,   0,   0,   0,   0
"Global macro history","rsm.configvalue[RSM.SLV.DNS.NS.UPD]"         ,60,1611252002,  99,  99,  99,  99
"Global macro history","rsm.configvalue[RSM.SLV.DNS.UDP.RTT]"        ,60,1611252056,   5,   5,   5,   5
"Global macro history","rsm.configvalue[RSM.SLV.NS.DOWNTIME]"        ,60,1611252058, 432, 432, 432, 432

"Probe statuses","online.nodes.pl[total,dns]",300,1611252205,2
"Probe statuses","online.nodes.pl[total,ip4]",300,1611252207,2
"Probe statuses","online.nodes.pl[total,ip6]",300,1611252208,2
"Probe statuses","online.nodes.pl[online,dns]",60,1611252020,2,2,2,2
"Probe statuses","online.nodes.pl[online,ip4]",60,1611252022,2,2,2,2
"Probe statuses","online.nodes.pl[online,ip6]",60,1611252023,2,2,2,2

"Probe1-Server1 - mon","rsm.probe.online"                          ,60,1611252000,         1,         1,         1,         1
"Probe1-Server1 - mon","zabbix[proxy,{$RSM.PROXY_NAME},lastaccess]",60,1611252045,1611252044,1611252104,1611252164,1611252224
"Probe2-Server1 - mon","rsm.probe.online"                          ,60,1611252000,         1,         1,         1,         1
"Probe2-Server1 - mon","zabbix[proxy,{$RSM.PROXY_NAME},lastaccess]",60,1611252045,1611252044,1611252104,1611252164,1611252224

"tld2 Probe1-Server1","rsm.dns.mode"                        ,60,1611252030,0,0,0,0
"tld2 Probe1-Server1","rsm.dns.status"                      ,60,1611252030,1,1,1,1
"tld2 Probe1-Server1","rsm.dnssec.status"                   ,60,1611252030,1,1
"tld2 Probe1-Server1","rsm.dns.ns.status[ns1.tld2]"         ,60,1611252030,1,1,1,1
"tld2 Probe1-Server1","rsm.dns.protocol"                    ,60,1611252030,0,0,0,0
"tld2 Probe1-Server1","rsm.dns.rtt[ns1.tld2,192.0.2.31,udp]",60,1611252030,5,6,7,8

"tld2 Probe2-Server1","rsm.dns.mode"                        ,60,1611252030,0,0,0,0
"tld2 Probe2-Server1","rsm.dns.status"                      ,60,1611252030,1,1,1,1
"tld2 Probe2-Server1","rsm.dnssec.status"                   ,60,1611252030,1,1
"tld2 Probe2-Server1","rsm.dns.ns.status[ns1.tld2]"         ,60,1611252030,1,1,1,1
"tld2 Probe2-Server1","rsm.dns.protocol"                    ,60,1611252030,0,0,0,0
"tld2 Probe2-Server1","rsm.dns.rtt[ns1.tld2,192.0.2.31,udp]",60,1611252030,8,7,6,5

"tld2 Probe1-Server1","rsm.dns.testedname",60,1611252048,test.tld2.,test.tld2.,test.tld2.,test.tld2.
"tld2 Probe2-Server1","rsm.dns.testedname",60,1611252018,test.tld2.,test.tld2.,test.tld2.,test.tld2.

"tld2","dns.udp.enabled",60,1611252054,1,1,1,1
"tld2","dnssec.enabled" ,60,1611252054,1,1,0,0

[fix-lastvalue-tables]

[set-lastvalue]

"tld2","rsm.slv.dns.avail"                        ,"${ts:2021-01-21 17:59:00}","0"
"tld2","rsm.slv.dns.rollweek"                     ,"${ts:2021-01-21 17:59:00}","0"
"tld2","rsm.slv.dnssec.avail"                     ,"${ts:2021-01-21 17:59:00}","0"
"tld2","rsm.slv.dnssec.rollweek"                  ,"${ts:2021-01-21 17:59:00}","0"
"tld2","rsm.slv.dns.ns.avail[ns1.tld2,192.0.2.31]","${ts:2021-01-21 17:59:00}","0"

[empty-directory]

"/opt/zabbix/sla"
"/opt/zabbix/sla-tmp"
"/opt/zabbix/cache"
"/opt/zabbix/data"

[set-host-macro]

"Template Rsmhost Config tld2","{$RSM.TLD.DNSSEC.ENABLED}",0

[start-server]

"2021-01-23 00:00:00"

[execute]

"2021-01-21 18:30:00","/opt/zabbix/scripts/slv/rsm.slv.dns.avail.pl       --cycles 4"
"2021-01-21 18:30:00","/opt/zabbix/scripts/slv/rsm.slv.dns.rollweek.pl    --cycles 4"
"2021-01-21 18:30:00","/opt/zabbix/scripts/slv/rsm.slv.dnssec.avail.pl    --cycles 4"
"2021-01-21 18:30:00","/opt/zabbix/scripts/slv/rsm.slv.dnssec.rollweek.pl --cycles 4"
"2021-01-21 18:30:00","/opt/zabbix/scripts/slv/rsm.slv.dns.ns.avail.pl    --cycles 4"

[stop-server]

[execute]

"","echo -n ${ts:2021-01-21 17:59:59} > /opt/zabbix/sla/last_update.txt"

"2021-01-21 17:59:00","/opt/zabbix/scripts/sla-api-recent.pl  --prettify-json --max-children 1 --print-period --period 4"
"2021-01-21 18:08:00","/opt/zabbix/scripts/update-api-data.pl --prettify-json --max-children 1 --print-period --period 4 --continue"

[compare-files]

"/opt/zabbix/sla","028-sla-output.tar.gz"

[compare-file]

"/opt/zabbix/cache/sla-api/server_1.json","${file:028-output-cache-server_1.json}"

[test-case]

"RDAP - dynamic domain name 2"

[start-tool]

"resolver","/tmp/resolver.pid","000-resolver.json"
"web-server","/tmp/web-server.pid","004-rdap-server.json"

[execute]

"","rm -rf   /tmp/dynamic-rdap-test"
"","mkdir -p /tmp/dynamic-rdap-test"
"2022-01-01 00:07:25","${cfg:paths:build_dir}/bin/t_rsm_rdap -r 127.0.0.1 -o 5053 -u http://example.example:4380/rdap -d static-domain -4 -l '${ts:2022-01-01 00:00:00}=dynamic-domain-1.example,${ts:2022-01-01 00:05:00}=dynamic-domain-2.example' -j /tmp/dynamic-rdap-test/status.json"

[stop-tool]

"web-server","/tmp/web-server.pid"
"resolver","/tmp/resolver.pid"

[compare-file]

"/tmp/dynamic-rdap-test/status.json","/${file:004-test.out}/"

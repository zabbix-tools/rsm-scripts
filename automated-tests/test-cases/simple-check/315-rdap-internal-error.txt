[test-case]

"RDAP - Internal error when resolving host name (-2)"

[start-tool]

"resolver","/tmp/resolver.pid","000-resolver-config-no-v4-ips.json"

[execute]

"","rm -rf   /tmp/simple-check-test"
"","mkdir -p /tmp/simple-check-test"
"","${cfg:paths:build_dir}/bin/t_rsm_rdap -r 127.0.0.1 -o 5053 -u http://example.example:4380/rdap -d example.example -4 -j /tmp/simple-check-test/status.json"

[stop-tool]

"resolver","/tmp/resolver.pid"

[compare-file]

"/tmp/simple-check-test/status.json","${file:315-status-output.json}"

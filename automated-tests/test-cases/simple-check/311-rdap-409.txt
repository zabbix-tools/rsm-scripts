[test-case]

"RDAP - ldhName member doesn't match query in response (-409)"

[start-tool]

"resolver","/tmp/resolver.pid","000-resolver-config-example.example.json"
"web-server","/tmp/web-server.pid","311-rdap-server-config.json"

[execute]

"","rm -rf   /tmp/simple-check-test"
"","mkdir -p /tmp/simple-check-test"
"","${cfg:paths:build_dir}/bin/t_rsm_rdap -r 127.0.0.1 -o 5053 -u http://example.example:4380/rdap -d example.example -4 -j /tmp/simple-check-test/status.json"

[stop-tool]

"web-server","/tmp/web-server.pid"
"resolver","/tmp/resolver.pid"

[compare-file]

"/tmp/simple-check-test/status.json","${file:311-status-output.json}"

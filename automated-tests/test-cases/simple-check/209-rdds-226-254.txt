[test-case]

"RDDS43/RDDS80 - No IP Addresses found when resolving hostname (-226/-254)"

[start-tool]

"resolver","/tmp/resolver.pid","000-resolver-config-no-ips.json"

[execute]

"","rm -rf   /tmp/simple-check-test"
"","mkdir -p /tmp/simple-check-test"
"","${cfg:paths:build_dir}/bin/t_rsm_rdds -t example -a example.example -s 4343 -w http://example.example -g 4380 -p example -4 -r 127.0.0.1 -o 5053 -j /tmp/simple-check-test/status.json"

[stop-tool]

"resolver","/tmp/resolver.pid"

[compare-file]

"/tmp/simple-check-test/status.json","${file:209-status-output.json}"

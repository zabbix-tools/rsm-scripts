[test-case]

"RDDS - Extended logging of the RDDS80-enabled test"

[start-tool]

"web-server","/tmp/web-server.pid","000-rdds80-server-config-generic.json"
"resolver","/tmp/resolver.pid","000-resolver-config-rdds.json"

[execute]

"","rm -rf   /tmp/simple-check-test"
"","mkdir -p /tmp/simple-check-test"
"","${cfg:paths:build_dir}/bin/t_rsm_rdds -t example -w http://example.example -g 4380 -p example.example -4 -r 127.0.0.1 -o 5053 | tee /tmp/simple-check-test/test.out"

[stop-tool]

"resolver","/tmp/resolver.pid"
"web-server","/tmp/web-server.pid"

[compare-file]

"/tmp/simple-check-test/test.out","/${file:412-test.out}/"

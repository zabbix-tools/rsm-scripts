[test-case]

"[950] don't update RDDS43 NS string when rdds43NsString contains default value"

[prepare-server-database]

[set-global-macro]

"{$RSM.MONITORING.TARGET}","registry"

[set-variable]

"empty-rdds43-ns-string"  ,""
"default-rdds43-ns-string","Name Server:"
"custom-rdds43-ns-string" ,"Custom Name Server:"

[rsm-api]

"/tlds/tld01","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld01-1-default.json"

"/tlds/tld02","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld02-1-custom.json"

"/tlds/tld03","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld03-1-empty.json"

"/tlds/tld04","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld04-1-default.json"
"/tlds/tld04","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld04-2-custom.json"

"/tlds/tld05","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld05-1-default.json"
"/tlds/tld05","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld05-2-default.json"

"/tlds/tld06","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld06-1-custom.json"
"/tlds/tld06","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld06-2-custom.json"

"/tlds/tld07","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld07-1-custom.json"
"/tlds/tld07","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld07-2-custom.json"

"/tlds/tld08","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld08-1-empty.json"
"/tlds/tld08","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld08-2-default.json"

"/tlds/tld09","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld09-1-empty.json"
"/tlds/tld09","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld09-2-custom.json"

"/tlds/tld10","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld10-1-default.json"
"/tlds/tld10","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld10-2-custom.json"
"/tlds/tld10","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld10-3-custom.json"

"/tlds/tld11","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld11-1-default.json"
"/tlds/tld11","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld11-2-default.json"
"/tlds/tld11","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld11-3-custom.json"

"/tlds/tld12","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld12-1-custom.json"
"/tlds/tld12","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld12-2-custom.json"
"/tlds/tld12","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld12-3-custom.json"

"/tlds/tld13","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld13-1-custom.json"
"/tlds/tld13","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld13-2-custom.json"
"/tlds/tld13","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld13-3-custom.json"

"/tlds/tld14","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld14-1-empty.json"
"/tlds/tld14","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld14-2-default.json"
"/tlds/tld14","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld14-3-custom.json"

"/tlds/tld15","PUT",200,"readwrite","2xx-inputs/248-rdds43-disabled.json"         ,"2xx-outputs/248-tld15-1-empty.json"
"/tlds/tld15","PUT",200,"readwrite","2xx-inputs/248-rdds43-custom-ns-string.json" ,"2xx-outputs/248-tld15-2-custom.json"
"/tlds/tld15","PUT",200,"readwrite","2xx-inputs/248-rdds43-default-ns-string.json","2xx-outputs/248-tld15-3-custom.json"

[check-host-macro]

"Template Rsmhost Config tld01","{$RSM.RDDS.NS.STRING}","${default-rdds43-ns-string}"
"Template Rsmhost Config tld02","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config tld03","{$RSM.RDDS.NS.STRING}","${empty-rdds43-ns-string}"
"Template Rsmhost Config tld04","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config tld05","{$RSM.RDDS.NS.STRING}","${default-rdds43-ns-string}"
"Template Rsmhost Config tld06","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config tld07","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config tld08","{$RSM.RDDS.NS.STRING}","${default-rdds43-ns-string}"
"Template Rsmhost Config tld09","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config tld10","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config tld11","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config tld12","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config tld13","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config tld14","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config tld15","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"

[test-case]

"[950] don't update RDDS43 NS string when rdds43NsString contains default value"

[prepare-server-database]

[set-global-macro]

"{$RSM.MONITORING.TARGET}","registrar"

[set-variable]

"empty-rdds43-ns-string"  ,""
"default-rdds43-ns-string","Name Server:"
"custom-rdds43-ns-string" ,"Custom Name Server:"

[rsm-api]

"/registrars/1" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr01-1-default.json"

"/registrars/2" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr02-1-custom.json"

"/registrars/3" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr03-1-empty.json"

"/registrars/4" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr04-1-default.json"
"/registrars/4" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr04-2-custom.json"

"/registrars/5" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr05-1-default.json"
"/registrars/5" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr05-2-default.json"

"/registrars/6" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr06-1-custom.json"
"/registrars/6" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr06-2-custom.json"

"/registrars/7" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr07-1-custom.json"
"/registrars/7" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr07-2-custom.json"

"/registrars/8" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr08-1-empty.json"
"/registrars/8" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr08-2-default.json"

"/registrars/9" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr09-1-empty.json"
"/registrars/9" ,"PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr09-2-custom.json"

"/registrars/10","PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr10-1-default.json"
"/registrars/10","PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr10-2-custom.json"
"/registrars/10","PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr10-3-custom.json"

"/registrars/11","PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr11-1-default.json"
"/registrars/11","PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr11-2-default.json"
"/registrars/11","PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr11-3-custom.json"

"/registrars/12","PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr12-1-custom.json"
"/registrars/12","PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr12-2-custom.json"
"/registrars/12","PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr12-3-custom.json"

"/registrars/13","PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr13-1-custom.json"
"/registrars/13","PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr13-2-custom.json"
"/registrars/13","PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr13-3-custom.json"

"/registrars/14","PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr14-1-empty.json"
"/registrars/14","PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr14-2-default.json"
"/registrars/14","PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr14-3-custom.json"

"/registrars/15","PUT",200,"readwrite","3xx-inputs/336-rdds43-disabled.json"         ,"3xx-outputs/336-rr15-1-empty.json"
"/registrars/15","PUT",200,"readwrite","3xx-inputs/336-rdds43-custom-ns-string.json" ,"3xx-outputs/336-rr15-2-custom.json"
"/registrars/15","PUT",200,"readwrite","3xx-inputs/336-rdds43-default-ns-string.json","3xx-outputs/336-rr15-3-custom.json"

[check-host-macro]

"Template Rsmhost Config 1" ,"{$RSM.RDDS.NS.STRING}","${default-rdds43-ns-string}"
"Template Rsmhost Config 2" ,"{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config 3" ,"{$RSM.RDDS.NS.STRING}","${empty-rdds43-ns-string}"
"Template Rsmhost Config 4" ,"{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config 5" ,"{$RSM.RDDS.NS.STRING}","${default-rdds43-ns-string}"
"Template Rsmhost Config 6" ,"{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config 7" ,"{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config 8" ,"{$RSM.RDDS.NS.STRING}","${default-rdds43-ns-string}"
"Template Rsmhost Config 9" ,"{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config 10","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config 11","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config 12","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config 13","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config 14","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"
"Template Rsmhost Config 15","{$RSM.RDDS.NS.STRING}","${custom-rdds43-ns-string}"

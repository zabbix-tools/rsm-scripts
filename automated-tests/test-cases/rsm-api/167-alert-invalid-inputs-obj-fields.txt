[test-case]

"Alert, invalid inputs - object fields"

[execute]

# make sure that alerts directory exists and is writable by web server user

"","rm -rf ${cfg:rsm-api:alerts_dir}"
"","mkdir -m 777 ${cfg:rsm-api:alerts_dir}"

[rsm-api]

"/alerts/alert-1","POST",400,"alerts","1xx-invalid-alert-inputs/167-obj-value.json","1xx-alert-outputs/167-obj-value.json"

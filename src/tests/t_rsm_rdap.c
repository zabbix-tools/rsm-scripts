#include "t_rsm_decl.h"
#include "t_rsm.h"

#include "../zabbix_server/poller/checks_simple_rsm_rdap.c"

#define DEFAULT_RES_PORT	53
#define DEFAULT_MAXREDIRS	10

void	zbx_on_exit(int ret)
{
	ZBX_UNUSED(ret);
}

static void	exit_usage(const char *program)
{
	fprintf(stderr, "usage: %s -r <ip> [-o <res_port>] -u <base_url> -d <testedname> [-l <list>] <[-4 -6]> [-j <file>] [-h]\n",
			program);
	fprintf(stderr, "       -r <res_ip>                      local resolver IP\n");
	fprintf(stderr, "       -o <res_port>                    port of resolver to use (default: %hu)\n", DEFAULT_RES_PORT);
	fprintf(stderr, "       -u <base_url>                    RDAP service endpoint\n");
	fprintf(stderr, "       -d <testedname>                  static domain name to use if dynamic unspecified or not available\n");
	fprintf(stderr, "       -l <timestamp=testedname[,...]>  dynamic domain name list to use\n");
	fprintf(stderr, "       -a <directory>                   directory for storing rdapoutput files\n");
	fprintf(stderr, "       -4                               enable IPv4\n");
	fprintf(stderr, "       -6                               enable IPv6\n");
	fprintf(stderr, "       -h                               show this message and quit\n");
	fprintf(stderr, "       -j <file>                        write resulting json to the file\n");
	exit(EXIT_FAILURE);
}

int	main(int argc, char *argv[])
{
	int		c, index,
			maxredirs = DEFAULT_MAXREDIRS,
			res_port = DEFAULT_RES_PORT,
			nextcheck;
	char		*testedname_static = NULL, *testedname_dynamic_list = "", *base_url = NULL, *res_ip = NULL,
			*rdapoutput_directory = "", ipv4_enabled = 0, ipv6_enabled = 0, *json_file = NULL,
			key[8192],
			res_host_buf[RSM_BUF_SIZE];
	AGENT_REQUEST	request;
	AGENT_RESULT	result;

	nextcheck = (int)time(NULL);

	while ((c = getopt(argc, argv, "r:o:u:d:l:a:46j:h")) != -1)
	{
		switch (c)
		{
			case 'r':
				res_ip = optarg;
				break;
			case 'o':
				res_port = atoi(optarg);
				break;
			case 'u':
				base_url = optarg;
				break;
			case 'd':
				testedname_static = optarg;
				break;
			case 'l':
				testedname_dynamic_list = optarg;
				break;
			case 'a':
				rdapoutput_directory = optarg;
				break;
			case '4':
				ipv4_enabled = 1;
				break;
			case '6':
				ipv6_enabled = 1;
				break;
			case 'j':
				json_file = optarg;
				break;
			case 'h':
				exit_usage(argv[0]);
				/* fall through */
			case '?':
				if (optopt == 'r' || optopt == 'u' || optopt == 'd')
					fprintf(stderr, "Option -%c requires an argument.\n", optopt);
				else if (isprint (optopt))
					fprintf(stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf(stderr, "Unknown option character `\\x%d'.\n", optopt);
				exit(EXIT_FAILURE);
			default:
				abort();
		}
	}

	for (index = optind; index < argc; index++)
		printf("Non-option argument %s\n", argv[index]);

	if (NULL == res_ip || '\0' == *res_ip)
	{
		fprintf(stderr, "Name Server IP [-r] must be specified\n");
		exit_usage(argv[0]);
	}

	if (NULL == base_url || '\0' == *base_url)
	{
		fprintf(stderr, "Base URL [-u] must be specified\n");
		exit_usage(argv[0]);
	}

	if (NULL == testedname_static || '\0' == *testedname_static)
	{
		fprintf(stderr, "Static test domain [-d] must be specified\n");
		exit_usage(argv[0]);
	}

	if (0 == ipv4_enabled && 0 == ipv6_enabled)
	{
		fprintf(stderr, "at least one IP version [-4, -6] must be specified\n");
		exit_usage(argv[0]);
	}

	zbx_snprintf(res_host_buf,  sizeof(res_host_buf),  "%s;%d", res_ip, res_port);

	zbx_snprintf(key, sizeof(key), "rdap[%s,%s,\"%s\",%s,%d,%d,%d,%d,%d,%d,%s,%s]",
			"example",			/* Rsmhost */
			testedname_static,		/* static testdomain name */
			testedname_dynamic_list,	/* comma-separated list of dynamic testdomain names */
			base_url,			/* RDAP service endpoint */
			maxredirs,			/* maximal number of redirections allowed */
			10000,				/* {$RSM.RDAP.RTT.HIGH} */
			1,				/* RDAP enabled for TLD */
			1,				/* RDAP enabled for probe */
			ipv4_enabled,			/* IPv4 enabled */
			ipv6_enabled,			/* IPv6 enabled */
			res_host_buf,			/* IP address of local resolver */
			rdapoutput_directory		/* rdapoutput directory, if specified */
	);

	if (SUCCEED != parse_item_key(key, &request))
	{
		rsm_errf(stderr, "invalid item key format: %s", key);
		exit(EXIT_FAILURE);
	}

	init_result(&result);

	signal(SIGALRM, alarm_signal_handler);

	check_rsm_rdap("example Probe1", &request, &result, nextcheck, stdout);

	free_request(&request);

	if (ISSET_MSG(&result))
	{
		printf("FAILED: %s\n",  result.msg);
		free_result(&result);
		exit(EXIT_FAILURE);
	}

	if (json_file)
	{
		char	*error = NULL;
		int	rv;

		rsm_infof(stdout, "writing to %s...", json_file);

		rv = write_json_status(json_file, result.text, &error);

		zbx_free(error);
		free_result(&result);

		if (rv != SUCCEED)
			exit(EXIT_FAILURE);
	}

	free_result(&result);

	exit(EXIT_SUCCESS);
}

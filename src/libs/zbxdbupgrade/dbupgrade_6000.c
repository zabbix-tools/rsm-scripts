/*
** Zabbix
** Copyright (C) 2001-2022 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/

#include "common.h"
#include "db.h"
#include "dbupgrade.h"
#include "db.h"
#include "log.h"

/*
 * Some common helpers that can be used as one-liners in patches to avoid copy-pasting.
 *
 * Be careful when implementing new helpers - they have to be generic.
 * If some code is needed only 1-2 times, it doesn't fit here.
 * If some code depends on stuff that is likely to change, it doesn't fit here.
 *
 * If more specific helper is needed, it must be implemented close to the patch that needs it. Specific
 * helpers can be implemented either as functions right before DBpatch_5000xxx(), or as macros inside
 * the DBpatch_5000xxx(). If they're implemented as macros, don't forget to #undef them.
 */

/* checks if this is server; used for skipping patches when running on proxy */
#define ONLY_SERVER()													\
															\
do															\
{															\
	if (0 == (program_type & ZBX_PROGRAM_TYPE_SERVER))								\
	{														\
		return SUCCEED;												\
	}														\
}															\
while (0)

/* checks result of DBexecute() */
#define DB_EXEC(...)													\
															\
do															\
{															\
	int	__result = DBexecute(__VA_ARGS__);									\
	if (ZBX_DB_OK > __result)											\
	{														\
		zabbix_log(LOG_LEVEL_CRIT, "%s() on line %d: got unexpected result", __func__, __LINE__);		\
		goto out;												\
	}														\
}															\
while (0)

/* selects single value of zbx_uint64_t type from the database */
#define SELECT_VALUE_UINT64(target_variable, query, ...)								\
															\
do															\
{															\
	DB_RESULT	__result;											\
	DB_ROW		__row;												\
															\
	__result = DBselect(query, __VA_ARGS__);									\
															\
	/* check for errors */												\
	if (NULL == __result)												\
	{														\
		zabbix_log(LOG_LEVEL_CRIT, "%s() on line %d: query failed", __func__, __LINE__);			\
		goto out;												\
	}														\
															\
	__row = DBfetch(__result);											\
															\
	/* check if there's at least one row in the resultset */							\
	if (NULL == __row)												\
	{														\
		zabbix_log(LOG_LEVEL_CRIT, "%s() on line %d: query did not return any rows", __func__, __LINE__);	\
		DBfree_result(__result);										\
		goto out;												\
	}														\
															\
	ZBX_STR2UINT64(target_variable, __row[0]);									\
															\
	__row = DBfetch(__result);											\
															\
	/* check that there are no more rows in the resultset */							\
	if (NULL != __row)												\
	{														\
		zabbix_log(LOG_LEVEL_CRIT, "%s() on line %d: query returned more than one row", __func__, __LINE__);	\
		DBfree_result(__result);										\
		goto out;												\
	}														\
															\
	DBfree_result(__result);											\
}															\
while (0)

/* gets groupid of the host group */
#define GET_HOST_GROUP_ID(groupid, name)										\
		SELECT_VALUE_UINT64(groupid, "select groupid from hstgrp where name='%s'", name)

#define zbx_db_dyn_escape_string(src)	zbx_db_dyn_escape_string(src, ZBX_SIZE_T_MAX, ZBX_SIZE_T_MAX, ESCAPE_SEQUENCE_ON)

extern unsigned char	program_type;

/*
 * 6.0 maintenance database patches
 */

#ifndef HAVE_SQLITE3

static int	DBpatch_6000000(void)
{
	return SUCCEED;
}

/* 6000000, 1 - create {$RSM.PROXY.IP}, {$RSM.PROXY.PORT} macros */
static int	DBpatch_6000000_1(void)
{
	int		ret = FAIL;

	DB_RESULT	result = NULL;
	DB_ROW		row;

	ONLY_SERVER();

	result = DBselect("select"
				" hosts.host,"
				"hosts.status,"
				"interface.ip,"
				"interface.port"
			" from"
				" hosts"
				" left join interface on interface.hostid=hosts.hostid"
			" where"
				" hosts.status in (5,6)");

	if (NULL == result)
		goto out;

	while (NULL != (row = DBfetch(result)))
	{
		const char	*host;
		int		status;
		const char	*ip;
		const char	*port;
		zbx_uint64_t	templateid;

		host   = row[0];
		status = atoi(row[1]);
		ip     = row[2];
		port   = row[3];

		if (status != 6)
		{
			zabbix_log(LOG_LEVEL_CRIT, "%s() on line %d: proxy '%s' must be passive (enabled)", __func__, __LINE__, host);		\
			goto out;
		}

		SELECT_VALUE_UINT64(templateid, "select hostid from hosts where host='Template Probe Config %s'", host);

#define SQL	"insert into hostmacro set hostmacroid=" ZBX_FS_UI64 ",hostid=" ZBX_FS_UI64 ",macro='%s',value='%s',description='%s',type=0"
		DB_EXEC(SQL, DBget_maxid_num("hostmacro", 1), templateid, "{$RSM.PROXY.IP}", ip, "Proxy IP of the proxy");
		DB_EXEC(SQL, DBget_maxid_num("hostmacro", 1), templateid, "{$RSM.PROXY.PORT}", port, "Port of the proxy");
#undef SQL
	}

	ret = SUCCEED;
out:
	DBfree_result(result);

	return ret;
}

/* 6000000, 2 - create provisioning_api_log table */
static int	DBpatch_6000000_2(void)
{
	const ZBX_TABLE	table =
			{"provisioning_api_log", "provisioning_api_logid", 0,
				{
					{"provisioning_api_logid", NULL, NULL, NULL, 0  , ZBX_TYPE_ID  , ZBX_NOTNULL, 0},
					{"clock"                 , NULL, NULL, NULL, 0  , ZBX_TYPE_INT , ZBX_NOTNULL, 0},
					{"user"                  , NULL, NULL, NULL, 100, ZBX_TYPE_CHAR, ZBX_NOTNULL, 0},
					{"interface"             , NULL, NULL, NULL, 8  , ZBX_TYPE_CHAR, ZBX_NOTNULL, 0},
					{"identifier"            , NULL, NULL, NULL, 255, ZBX_TYPE_CHAR, ZBX_NOTNULL, 0},
					{"operation"             , NULL, NULL, NULL, 6  , ZBX_TYPE_CHAR, ZBX_NOTNULL, 0},
					{"object_type"           , NULL, NULL, NULL, 9  , ZBX_TYPE_CHAR, ZBX_NOTNULL, 0},
					{"object_before"         , NULL, NULL, NULL, 0  , ZBX_TYPE_TEXT, 0          , 0},
					{"object_after"          , NULL, NULL, NULL, 0  , ZBX_TYPE_TEXT, 0          , 0},
					{"remote_addr"           , NULL, NULL, NULL, 45 , ZBX_TYPE_CHAR, ZBX_NOTNULL, 0},
					{"x_forwarded_for"       , NULL, NULL, NULL, 255, ZBX_TYPE_CHAR, 0          , 0},
					{0}
				},
				NULL
			};

	return DBcreate_table(&table);
}

/* 6000000, 3 - remove {$RSM.EPP.ENABLED} and {$RSM.TLD.EPP.ENABLED} macros from rsm.dns[] and rsm.rdds[] items */
static int	DBpatch_6000000_3(void)
{
	int	ret = FAIL;

	ONLY_SERVER();

#define SQL	"update items set key_=replace(key_,',%s','') where key_ like '%s'"
	DB_EXEC(SQL, "{$RSM.TLD.EPP.ENABLED}", "rsm.dns[%]");
	DB_EXEC(SQL, "{$RSM.TLD.EPP.ENABLED}", "rsm.rdds[%]");
	DB_EXEC(SQL, "{$RSM.EPP.ENABLED}"    , "rsm.rdds[%]");
#undef SQL

	ret = SUCCEED;
out:
	return ret;
}

/* 6000000, 4 - rename {$RSM.TLD.RDDS.43.SERVERS} to {$RSM.TLD.RDDS43.SERVER} */
static int	DBpatch_6000000_4(void)
{
	int	ret = FAIL;

	DB_RESULT	result = NULL;
	DB_ROW		row;

	ONLY_SERVER();

	result = DBselect("select hostmacroid,value from hostmacro where macro='{$RSM.TLD.RDDS.43.SERVERS}'");

	if (NULL == result)
		goto out;

	while (NULL != (row = DBfetch(result)))
	{
		const char	*macroid = row[0];
		const char	*value   = row[1];

		if (NULL != strchr(value, ','))
		{
			zabbix_log(LOG_LEVEL_CRIT, "%s() on line %d: macro contains more than one server: '%s' (id: %s)",
					__func__, __LINE__, value, macroid);
			goto out;
		}

		DB_EXEC("update hostmacro set macro='{$RSM.TLD.RDDS43.SERVER}' where hostmacroid=%s", macroid);
	}

	ret = SUCCEED;
out:
	DBfree_result(result);

	return ret;
}

/* 6000000, 5 - rename {$RSM.TLD.RDDS.80.SERVERS} to {$RSM.TLD.RDDS80.URL} */
static int	DBpatch_6000000_5(void)
{
	int	ret = FAIL;

	DB_RESULT	result = NULL;
	DB_ROW		row;

	ONLY_SERVER();

	result = DBselect("select hostmacroid,value from hostmacro where macro='{$RSM.TLD.RDDS.80.SERVERS}'");

	if (NULL == result)
		goto out;

	while (NULL != (row = DBfetch(result)))
	{
		const char	*macroid = row[0];
		const char	*value   = row[1];

		if (NULL != strchr(value, ','))
		{
			zabbix_log(LOG_LEVEL_CRIT, "%s() on line %d: macro contains more than one server: '%s' (id: %s)",
					__func__, __LINE__, value, macroid);
			goto out;
		}

		DB_EXEC("update hostmacro set macro='{$RSM.TLD.RDDS80.URL}',value='http://%s/' where hostmacroid=%s",
				value, macroid);
	}

	ret = SUCCEED;
out:
	DBfree_result(result);

	return ret;
}

/* 6000000, 6 - update rsm.rdds[] items to use {$RSM.TLD.RDDS43.SERVER} and {$RSM.TLD.RDDS80.URL} */
static int	DBpatch_6000000_6(void)
{
	int	ret = FAIL;

	ONLY_SERVER();

#define SQL	"update items set key_=replace(key_,'%s','%s') where key_ like 'rsm.rdds[%%]'"
	DB_EXEC(SQL, "{$RSM.TLD.RDDS.43.SERVERS}", "{$RSM.TLD.RDDS43.SERVER}");
	DB_EXEC(SQL, "{$RSM.TLD.RDDS.80.SERVERS}", "{$RSM.TLD.RDDS80.URL}");
#undef SQL

	ret = SUCCEED;
out:
	return ret;
}

/* 6000000, 7 - split {$RSM.TLD.RDDS.ENABLED} macro into {$RSM.TLD.RDDS43.ENABLED} and {$RSM.TLD.RDDS80.ENABLED} */
static int	DBpatch_6000000_7(void)
{
	int		ret = FAIL;

	DB_RESULT	result = NULL;
	DB_ROW		row;

	ONLY_SERVER();

	result = DBselect("select hostmacroid,hostid,value,type from hostmacro where macro='{$RSM.TLD.RDDS.ENABLED}'");

	if (NULL == result)
		goto out;

#define SQL_UPDATE	"update hostmacro set macro='%s',description='%s' where hostmacroid=%s"
#define SQL_INSERT	"insert into hostmacro set hostmacroid=" ZBX_FS_UI64 ",hostid=%s,macro='%s',value='%s',description='%s',type='%s'"

#define RDDS43_MACRO	"{$RSM.TLD.RDDS43.ENABLED}"
#define RDDS80_MACRO	"{$RSM.TLD.RDDS80.ENABLED}"

#define RDDS43_DESCR	"Indicates whether RDDS43 is enabled on the rsmhost"
#define RDDS80_DESCR	"Indicates whether RDDS80 is enabled on the rsmhost"

	while (NULL != (row = DBfetch(result)))
	{
		const char	*hostmacroid = row[0];
		const char	*hostid      = row[1];
		const char	*value       = row[2];
		const char	*type        = row[3];

		DB_EXEC(SQL_UPDATE, RDDS43_MACRO, RDDS43_DESCR, hostmacroid);
		DB_EXEC(SQL_INSERT, DBget_maxid_num("hostmacro", 1), hostid, RDDS80_MACRO, value, RDDS80_DESCR, type);
	}

#undef RDDS43_MACRO
#undef RDDS80_MACRO

#undef RDDS43_DESCR
#undef RDDS80_DESCR

#undef SQL_UPDATE
#undef SQL_INSERT

	ret = SUCCEED;
out:
	DBfree_result(result);

	return ret;
}

/* 6000000, 8 - replace {$RSM.TLD.RDDS.ENABLED} macro with {$RSM.TLD.RDDS43.ENABLED} and {$RSM.TLD.RDDS80.ENABLED} in rsm.dns[] and rsm.rdds[] item keys */
static int	DBpatch_6000000_8(void)
{
	int	ret = FAIL;

	ONLY_SERVER();

#define FROM	",{$RSM.TLD.RDDS.ENABLED},"
#define TO	",{$RSM.TLD.RDDS43.ENABLED},{$RSM.TLD.RDDS80.ENABLED},"

	DB_EXEC("update items set key_=replace(key_,'%s','%s') where key_ like 'rsm.dns[%%]'", FROM, TO);
	DB_EXEC("update items set key_=replace(key_,'%s','%s') where key_ like 'rsm.rdds[%%]'", FROM, TO);

#undef FROM
#undef TO

	ret = SUCCEED;
out:
	return ret;
}

/* 6000000, 9 - split rdds.enabled item into rdds43.enabled and rdds80.enabled */
static int	DBpatch_6000000_9(void)
{
	int		ret = FAIL;

	DB_RESULT	result = NULL;
	DB_ROW		row;

	zbx_uint64_t	rdds43_templateid;
	zbx_uint64_t	rdds80_templateid;

	ONLY_SERVER();

	/* update rdds43.enabled template item */

	SELECT_VALUE_UINT64(rdds43_templateid, "select itemid from items where templateid is null and key_='%s'", "rdds.enabled");

#define SQL	"update"												\
			" items"											\
		" set"													\
			" name='RDDS43 enabled/disabled',"								\
			"key_='rdds43.enabled',"									\
			"params='{$RSM.TLD.RDDS43.ENABLED}',"								\
			"description='History of RDDS43 being enabled or disabled.'"					\
		" where"												\
			" itemid=" ZBX_FS_UI64

	DB_EXEC(SQL, rdds43_templateid);

#undef SQL

	/* create rdds80.enabled template item */

	rdds80_templateid = DBget_maxid_num("items", 1);

#define SQL	"insert into items ("											\
			"itemid,type,snmp_oid,hostid,name,key_,delay,history,trends,status,value_type,"			\
			"trapper_hosts,units,formula,logtimefmt,templateid,valuemapid,params,ipmi_sensor,authtype,"	\
			"username,password,publickey,privatekey,flags,interfaceid,description,inventory_link,lifetime,"	\
			"evaltype,jmx_endpoint,master_itemid,timeout,url,query_fields,posts,status_codes,"		\
			"follow_redirects,post_type,http_proxy,headers,retrieve_mode,request_method,output_format,"	\
			"ssl_cert_file,ssl_key_file,ssl_key_password,verify_peer,verify_host,allow_traps,discover"	\
		")"													\
		" select"												\
			" " ZBX_FS_UI64 ",type,snmp_oid,hostid,'%s','%s',delay,history,trends,status,value_type,"	\
			"trapper_hosts,units,formula,logtimefmt,templateid,valuemapid,'%s',ipmi_sensor,authtype,"	\
			"username,password,publickey,privatekey,flags,interfaceid,'%s',inventory_link,lifetime,"	\
			"evaltype,jmx_endpoint,master_itemid,timeout,url,query_fields,posts,status_codes,"		\
			"follow_redirects,post_type,http_proxy,headers,retrieve_mode,request_method,output_format,"	\
			"ssl_cert_file,ssl_key_file,ssl_key_password,verify_peer,verify_host,allow_traps,discover"	\
		" from items where itemid=" ZBX_FS_UI64

	DB_EXEC(SQL, rdds80_templateid, "RDDS80 enabled/disabled", "rdds80.enabled", "{$RSM.TLD.RDDS80.ENABLED}",
		"History of RDDS80 being enabled or disabled.", rdds43_templateid);

#undef SQL

	/* update rdds43.enabled items */

#define SQL	"update"												\
			" items"											\
		" set"													\
			" name='RDDS43 enabled/disabled',"								\
			" key_='rdds43.enabled',"									\
			" params='{$RSM.TLD.RDDS43.ENABLED}',"								\
			" description='History of RDDS43 being enabled or disabled.'"					\
		" where"												\
			" templateid=" ZBX_FS_UI64

	DB_EXEC(SQL, rdds43_templateid);

#undef SQL

	/* create rdds80.enabled items */

	result = DBselect("select itemid from items where templateid=" ZBX_FS_UI64, rdds43_templateid);

	if (NULL == result)
		goto out;

#define SQL_ITEMS													\
		"insert into items ("											\
			"itemid,type,snmp_oid,hostid,name,key_,delay,history,trends,status,value_type,"			\
			"trapper_hosts,units,formula,logtimefmt,templateid,valuemapid,params,ipmi_sensor,authtype,"	\
			"username,password,publickey,privatekey,flags,interfaceid,description,inventory_link,lifetime,"	\
			"evaltype,jmx_endpoint,master_itemid,timeout,url,query_fields,posts,status_codes,"		\
			"follow_redirects,post_type,http_proxy,headers,retrieve_mode,request_method,output_format,"	\
			"ssl_cert_file,ssl_key_file,ssl_key_password,verify_peer,verify_host,allow_traps,discover"	\
		")"													\
		" select"												\
			" " ZBX_FS_UI64 ",type,snmp_oid,hostid,'%s','%s',delay,history,trends,status,value_type,"	\
			"trapper_hosts,units,formula,logtimefmt," ZBX_FS_UI64 ",valuemapid,'%s',ipmi_sensor,authtype,"	\
			"username,password,publickey,privatekey,flags,interfaceid,'%s',inventory_link,lifetime,"	\
			"evaltype,jmx_endpoint,master_itemid,timeout,url,query_fields,posts,status_codes,"		\
			"follow_redirects,post_type,http_proxy,headers,retrieve_mode,request_method,output_format,"	\
			"ssl_cert_file,ssl_key_file,ssl_key_password,verify_peer,verify_host,allow_traps,discover"	\
		" from items where itemid=" ZBX_FS_UI64

#define SQL_RTDATA													\
		"insert into item_rtdata (itemid,lastlogsize,state,mtime,error)"					\
		"select " ZBX_FS_UI64 ",lastlogsize,state,mtime,error from item_rtdata where itemid=" ZBX_FS_UI64

#define SQL_HISTORY													\
		"insert into history_uint (itemid,clock,value,ns)"							\
		"select " ZBX_FS_UI64 ",clock,value,ns from history_uint where itemid=" ZBX_FS_UI64

	while (NULL != (row = DBfetch(result)))
	{
		zbx_uint64_t	rdds43_itemid;
		zbx_uint64_t	rdds80_itemid;

		ZBX_STR2UINT64(rdds43_itemid, row[0]);
		rdds80_itemid = DBget_maxid_num("items", 1);

		/* copy item */
		DB_EXEC(SQL_ITEMS,
			rdds80_itemid,
			"RDDS80 enabled/disabled",
			"rdds80.enabled",
			rdds80_templateid,
			"{$RSM.TLD.RDDS80.ENABLED}",
			"History of RDDS80 being enabled or disabled.",
			rdds43_itemid);

		/* copy rtdata */
		DB_EXEC(SQL_RTDATA, rdds80_itemid, rdds43_itemid);

		/* copy history */
		DB_EXEC(SQL_HISTORY, rdds80_itemid, rdds43_itemid);
	}

#undef SQL_ITEMS
#undef SQL_RTDATA
#undef SQL_HISTORY

	ret = SUCCEED;
out:
	DBfree_result(result);

	return ret;
}

/* 6000000, 10 - replace obsoleted positional macros $1 and $2 in item names */
static int	DBpatch_6000000_10(void)
{
	int		ret = FAIL;

	DB_RESULT	result = NULL;
	DB_ROW		row;

	ONLY_SERVER();

#define SQL	"update items"								\
		" set name=replace("							\
			"name"								\
			", '%s'"							\
			", substring_index("						\
				"substring_index("					\
					"regexp_substr(key_, '(?<=\\\\[).*(?=\\\\])')"	\
				", ',', %d)"						\
			", ',', -1)"							\
		")"									\
		" where key_ like 'rsm.configvalue[%%'"					\
			" or key_ like 'probe.configvalue[%%'"				\
			" or key_ like 'resolver.status[%%'"				\
			" or key_ like 'rsm.probe.status[%%'"				\
			" or key_ like 'rsm.slv.dns.ns.avail[%%'"			\
			" or key_ like 'rsm.slv.dns.ns.downtime[%%'"

	/* replace positional macros $1 and $2 */
	DB_EXEC(SQL, "$1", 1);
	DB_EXEC(SQL, "$2", 2);

#undef SQL

	/* make sure we handled everything */
	result = DBselect("select count(*) from items where name like '%%$1%%' or name like '%%$2%%'");

	if (NULL == result)
		goto out;

	if (NULL == (row = DBfetch(result)))
		goto out;

	if (0 != atoi(row[0]))
	{
		zabbix_log(LOG_LEVEL_CRIT, "%s() on line %d: positional macros left after trying to replace them",
				__func__, __LINE__);
		goto out;
	}

	ret = SUCCEED;
out:
	DBfree_result(result);

	return ret;
}

/* 6000000, 11 - register Provisioning API module and create its users */
static int	DBpatch_6000000_11(void)
{
	int		ret = FAIL;

	zbx_uint64_t	userid_ro, userid_rw, roleid, usrgrpid, id_ro, id_rw, moduleid;

	userid_ro = DBget_maxid_num("users"       , 2);
	userid_rw = userid_ro + 1;
	usrgrpid  = DBget_maxid_num("usrgrp"      , 1);
	id_ro     = DBget_maxid_num("users_groups", 2);
	id_rw     = id_ro + 1;
	moduleid  = DBget_maxid_num("module"      , 1);

	ONLY_SERVER();

	SELECT_VALUE_UINT64(roleid, "select roleid from role where name='%s'", "Super admin role");

#define SQL	"insert into users set userid=" ZBX_FS_UI64 ",username='%s',passwd='%s',autologout=0,roleid=" ZBX_FS_UI64
	DB_EXEC(SQL, userid_ro, "provisioning-api-readonly",  "5f4dcc3b5aa765d61d8327deb882cf99", roleid);
	DB_EXEC(SQL, userid_rw, "provisioning-api-readwrite", "5f4dcc3b5aa765d61d8327deb882cf99", roleid);
#undef SQL

#define SQL	"insert into usrgrp set usrgrpid=" ZBX_FS_UI64 ",name='%s',gui_access=3,users_status=0,debug_mode=0"
	DB_EXEC(SQL, usrgrpid, "Provisioning API");
#undef SQL

#define SQL	"insert into users_groups set id=" ZBX_FS_UI64 ",usrgrpid=" ZBX_FS_UI64 ",userid=" ZBX_FS_UI64
	DB_EXEC(SQL, id_ro, usrgrpid, userid_ro);
	DB_EXEC(SQL, id_rw, usrgrpid, userid_rw);
#undef SQL

#define SQL "insert into module set moduleid=" ZBX_FS_UI64 ",id='%s',relative_path='%s',status=1,config='[]'"
	DB_EXEC(SQL, moduleid, "RSM Provisioning API", "RsmProvisioningApi");
#undef SQL

	ret = SUCCEED;
out:
	return ret;
}

/* 6000000, 12 - create a template for storing value maps */
static int	DBpatch_6000000_12(void)
{
	int		ret = FAIL;

	DB_RESULT	valuemap_result = NULL;
	DB_ROW		valuemap_row;
	DB_RESULT	mapping_result = NULL;
	DB_ROW		mapping_row;

	zbx_uint64_t	groupid;
	zbx_uint64_t	hostid;
	const char	*template_name = "Template Value Maps";
	char		*template_uuid = NULL;
	char		*valuemap_name = NULL;
	char		*valuemap_uuid = NULL;
	char		*old_value = NULL;
	char		*new_value = NULL;

	ONLY_SERVER();

	GET_HOST_GROUP_ID(groupid, "Templates");
	hostid = DBget_maxid_num("hosts", 1);
	template_uuid = zbx_gen_uuid4(template_name);

	/* status 3 = HOST_STATUS_TEMPLATE */

	DB_EXEC("insert into hosts set"
			" hostid=" ZBX_FS_UI64 ",created=0,proxy_hostid=NULL,host='%s',status=%d,lastaccess=0,"
			"ipmi_authtype=-1,ipmi_privilege=2,ipmi_username='',ipmi_password='',maintenanceid=NULL,"
			"maintenance_status=0,maintenance_type=0,maintenance_from=0,name='%s',info_1='',info_2='',"
			"flags=0,templateid=NULL,description='',tls_connect=1,tls_accept=1,tls_issuer='',"
			"tls_subject='',tls_psk_identity='',tls_psk='',proxy_address='',auto_compress=1,discover=0,"
			"custom_interfaces=0,uuid='%s'",
		hostid, template_name, HOST_STATUS_TEMPLATE, template_name, template_uuid);

	DB_EXEC("insert into hosts_groups set"
			" hostgroupid=" ZBX_FS_UI64 ",hostid=" ZBX_FS_UI64 ",groupid=" ZBX_FS_UI64,
		DBget_maxid_num("hosts_groups", 1), hostid, groupid);

	valuemap_result = DBselect("select valuemapid,name from valuemaps_tmp order by valuemapid");

	if (NULL == valuemap_result)
		goto out;

	while (NULL != (valuemap_row = DBfetch(valuemap_result)))
	{
		zbx_uint64_t	valuemapid_old;
		zbx_uint64_t	valuemapid_new;
		unsigned int	sortorder = 0;

		ZBX_STR2UINT64(valuemapid_old, valuemap_row[0]);
		valuemap_name = DBdyn_escape_string(valuemap_row[1]);

		valuemapid_new = DBget_maxid_num("valuemap", 1);
		valuemap_uuid = zbx_gen_uuid4(valuemap_name);

#define SQL	"insert into valuemap set valuemapid=" ZBX_FS_UI64 ",hostid=" ZBX_FS_UI64 ",name='%s',uuid='%s'"
		DB_EXEC(SQL, valuemapid_new, hostid, valuemap_name, valuemap_uuid);
#undef SQL

		mapping_result = DBselect("select value,newvalue from mappings_tmp where valuemapid=" ZBX_FS_UI64 " order by mappingid", valuemapid_old);

		if (NULL == mapping_result)
			goto out;

		while (NULL != (mapping_row = DBfetch(mapping_result)))
		{
			old_value = DBdyn_escape_string(mapping_row[0]);
			new_value = DBdyn_escape_string(mapping_row[1]);

			/* type 0 = VALUEMAP_MAPPING_TYPE_EQUAL */

			DB_EXEC("insert into valuemap_mapping set"
					" valuemap_mappingid=" ZBX_FS_UI64 ",valuemapid=" ZBX_FS_UI64 ",value='%s',"
					"newvalue='%s',type=0,sortorder=%u",
				DBget_maxid_num("valuemap_mapping", 1), valuemapid_new, old_value, new_value, sortorder++);

			zbx_free(old_value);
			zbx_free(new_value);
		}

		zbx_free(valuemap_name);
		zbx_free(valuemap_uuid);
	}

	DB_EXEC("drop table valuemaps_tmp");
	DB_EXEC("drop table mappings_tmp");

	ret = SUCCEED;
out:
	DBfree_result(valuemap_result);
	DBfree_result(mapping_result);
	zbx_free(template_uuid);
	zbx_free(valuemap_name);
	zbx_free(valuemap_uuid);
	zbx_free(old_value);
	zbx_free(new_value);

	return ret;
}

/* 6000000, 13 - enable show_technical_errors */
static int	DBpatch_6000000_13(void)
{
	int	ret = FAIL;

	DB_EXEC("update config set show_technical_errors=1");

	ret = SUCCEED;
out:
	return ret;
}

/* 6000000, 14 - reset items.lifetime and items.request_method to default values */
static int	DBpatch_6000000_14(void)
{
	int	ret = FAIL;

	DB_EXEC("update items set lifetime=default(lifetime) where lifetime<>default(lifetime)");
	DB_EXEC("update items set request_method=default(request_method) where request_method<>default(request_method)");

	ret = SUCCEED;
out:
	return ret;
}

/* 6000000, 15 - add missing macros - {$RDAP.BASE.URL}, {$RDAP.TEST.DOMAIN}, {$RSM.RDDS43.TEST.DOMAIN} */
static int	DBpatch_6000000_15(void)
{
	int		ret = FAIL;

	DB_RESULT	result = NULL;
	DB_ROW		row;
	unsigned int 	i;

	const char	*macros[][2] = {
		{"{$RDAP.BASE.URL}"         , "Base URL for RDAP queries, e.g. http://whois.zabbix"},
		{"{$RDAP.TEST.DOMAIN}"      , "Test domain for RDAP queries, e.g. whois.zabbix"},
		{"{$RSM.RDDS43.TEST.DOMAIN}", "Domain name to use when querying RDDS43 server, e.g. \"whois.example\""},
	};

	ONLY_SERVER();

	for (i = 0; i < sizeof(macros) / sizeof(*macros); i++)
	{
		const char	*macro = macros[i][0];
		const char	*description = macros[i][1];

		result = DBselect("select"
					" hosts.hostid"
				" from"
					" hosts"
					" left join hostmacro on"
						" hostmacro.hostid=hosts.hostid and"
						" hostmacro.macro='%s'"
				" where"
					" hosts.host like 'Template Rsmhost Config %%' and"
					" hostmacro.hostmacroid is null",
				macro);

		if (NULL == result)
			goto out;

		while (NULL != (row = DBfetch(result)))
		{
			zbx_uint64_t	hostid;

			ZBX_STR2UINT64(hostid, row[0]);

#define SQL	"insert into hostmacro set hostmacroid=" ZBX_FS_UI64 ",hostid=" ZBX_FS_UI64 ",macro='%s',value='',description='%s',type=0"
			DB_EXEC(SQL, DBget_maxid_num("hostmacro", 1), hostid, macro, description);
#undef SQL
		}

		DBfree_result(result);
		result = NULL;
	}

	ret = SUCCEED;
out:
	DBfree_result(result);

	return ret;
}

/* 6000000, 16 - create table rsm_false_positive, this is required by frontend */
static int	DBpatch_6000000_16(void)
{
	const ZBX_TABLE	table =
			{"rsm_false_positive", "rsm_false_positiveid", 0,
				{
					{"rsm_false_positiveid", NULL, NULL, NULL, 0  , ZBX_TYPE_ID  , ZBX_NOTNULL, 0},
					{"userid"              , NULL, NULL, NULL, 0  , ZBX_TYPE_ID  , ZBX_NOTNULL, 0},
					{"eventid"             , NULL, NULL, NULL, 0  , ZBX_TYPE_ID  , ZBX_NOTNULL, 0},
					{"clock"               , NULL, NULL, NULL, 0  , ZBX_TYPE_INT , ZBX_NOTNULL, 0},
					{"status"              , NULL, NULL, NULL, 0  , ZBX_TYPE_INT , ZBX_NOTNULL, 0},
					{0}
				},
				NULL
			};

	return DBcreate_table(&table);
}

/* 6000000, 17 - add userid index to table rsm_false_positive */
static int	DBpatch_6000000_17(void)
{
	return DBcreate_index("rsm_false_positive", "rsm_false_positive_1", "userid", 0);
}

/* 6000000, 18 - add userid foreign key to table rsm_false_positive */
static int	DBpatch_6000000_18(void)
{
	const ZBX_FIELD field = {"userid", NULL, "users", "userid", 0, 0, 0, 0};

	return DBadd_foreign_key("rsm_false_positive", 1, &field);
}

/* 6000000, 19 - add eventid index to table rsm_false_positive */
static int	DBpatch_6000000_19(void)
{
	return DBcreate_index("rsm_false_positive", "rsm_false_positive_2", "eventid", 0);
}

/* 6000000, 20 - add eventid foreign key to table rsm_false_positive */
static int	DBpatch_6000000_20(void)
{
	const ZBX_FIELD field = {"eventid", NULL, "events", "eventid", 0, 0, 0, ZBX_FK_CASCADE_DELETE};

	return DBadd_foreign_key("rsm_false_positive", 2, &field);
}

/* 6000000, 21 - drop column events.false_positive */
static int	DBpatch_6000000_21(void)
{
	return DBdrop_field("events", "false_positive");
}

/* 6000000, 22 - add settings for "Read-only user" and "Power user" roles */
static int	DBpatch_6000000_22(void)
{
	/* this is just a direct paste from data.tmpl, with each line quoted and properly indented */
	static const char	*const data[] = {
		"ROW   |10000      |100   |0   |ui.monitoring.hosts   |1        |         |NULL          |NULL           |",
		"ROW   |10001      |100   |0   |ui.default_access     |0        |         |NULL          |NULL           |",
		"ROW   |10002      |100   |0   |services.read         |0        |         |NULL          |NULL           |",
		"ROW   |10003      |100   |0   |services.write        |0        |         |NULL          |NULL           |",
		"ROW   |10004      |100   |0   |modules.default_access|0        |         |NULL          |NULL           |",
		"ROW   |10005      |100   |0   |api.access            |0        |         |NULL          |NULL           |",
		"ROW   |10006      |100   |0   |actions.default_access|0        |         |NULL          |NULL           |",
		"ROW   |10007      |100   |2   |modules.module.0      |0        |         |1             |NULL           |",
		"ROW   |10008      |110   |0   |ui.monitoring.hosts   |1        |         |NULL          |NULL           |",
		"ROW   |10009      |110   |0   |ui.default_access     |0        |         |NULL          |NULL           |",
		"ROW   |10010      |110   |0   |services.read         |0        |         |NULL          |NULL           |",
		"ROW   |10011      |110   |0   |services.write        |0        |         |NULL          |NULL           |",
		"ROW   |10012      |110   |0   |modules.default_access|0        |         |NULL          |NULL           |",
		"ROW   |10013      |110   |0   |api.access            |0        |         |NULL          |NULL           |",
		"ROW   |10014      |110   |0   |actions.default_access|0        |         |NULL          |NULL           |",
		"ROW   |10015      |110   |2   |modules.module.0      |0        |         |1             |NULL           |",
		NULL
	};
	int			i;

	ONLY_SERVER();

	for (i = 0; NULL != data[i]; i++)
	{
		zbx_uint64_t	role_ruleid, roleid;
		char		*name = NULL, *value_str = NULL, *value_moduleid = NULL, *name_esc;
		int		type, value_int;

		if (0 == strncmp(data[i], "--", ZBX_CONST_STRLEN("--")))
			continue;

		if (7 != sscanf(data[i], "ROW |" ZBX_FS_UI64 " |" ZBX_FS_UI64 " |%d |%m[^|]|%d |%m[^|]|%m[^|]|",
				&role_ruleid, &roleid, &type, &name, &value_int, &value_str, &value_moduleid))
		{
			zabbix_log(LOG_LEVEL_CRIT, "failed to parse the following line:\n%s", data[i]);
			zbx_free(name);
			zbx_free(value_str);
			zbx_free(value_moduleid);
			return FAIL;
		}

		/* this one is unused */
		zbx_free(value_str);

		zbx_rtrim(name, ZBX_WHITESPACE);
		zbx_rtrim(value_moduleid, ZBX_WHITESPACE);

		/* NOTE: to keep it simple assume that data does not contain sequences "&pipe;", "&eol;" or "&bsn;" */

		name_esc = zbx_db_dyn_escape_string(name);
		zbx_free(name);

		if (ZBX_DB_OK > DBexecute(
				"insert into role_rule (role_ruleid,roleid,type,name,value_int,value_moduleid)"
				" values (" ZBX_FS_UI64 "," ZBX_FS_UI64 ",%d,'%s',%d,%s)",
				role_ruleid, roleid, type, name_esc, value_int, value_moduleid))
		{
			zbx_free(name_esc);
			zbx_free(value_moduleid);
			return FAIL;
		}

		zbx_free(name_esc);
		zbx_free(value_moduleid);
	}

	if (ZBX_DB_OK > DBexecute("delete from ids where table_name='role_rule'"))
		return FAIL;

	return SUCCEED;
}

/* 6000000, 23 add script "DNSViz webhook" */
static int	DBpatch_6000000_23(void)
{
	zbx_uint64_t	scriptid;
	int		ret = FAIL;

	ONLY_SERVER();

	scriptid = DBget_maxid_num("scripts", 1);

	DB_EXEC(
			"insert into scripts"
			" set scriptid=" ZBX_FS_UI64 ",name='DNSViz webhook',command='%s',host_access=2,description=''"
			",type=5,execute_on=2,timeout='1m',scope=1,authtype=0",
			scriptid,
			"try {\r\n"
			"    var req = new HttpRequest(), response, script_params = JSON.parse(value), uri_params = {}, payload;\r\n"
			"\r\n"
			"    // Set up headers.\r\n"
			"    req.addHeader(''Content-Type: application/x-www-form-urlencoded; charset=UTF-8'');\r\n"
			"    req.addHeader(''Cache-Control: no-cache'');\r\n"
			"    req.addHeader(''Connection: keep-alive'');\r\n"
			"    req.addHeader(''Pragma: no-cache'');\r\n"
			"\r\n"
			"    // These ones are important in order to get HTTP status code 200 instead of 403.\r\n"
			"    req.addHeader(''Referer: '' + script_params.url);\r\n"
			"    req.addHeader(''X-Requested-With: XMLHttpRequest'');\r\n"
			"\r\n"
			"    // Form elements.\r\n"
			"    payload = ''force_ancestor=.&analysis_type=0&perspective=server'';\r\n"
			"\r\n"
			"    Zabbix.log(3, ''[ DNSViz webhook ] POST \"'' + script_params.url + ''\" with \"'' + payload + ''\"'');\r\n"
			"\r\n"
			"    // Perform the POST request.\r\n"
			"    response = req.post(script_params.url, payload);\r\n"
			"\r\n"
			"    Zabbix.log(3, ''[ DNSViz webhook ] Responded with code: '' + req.getStatus() + ''. Response: '' + response);\r\n"
			"\r\n"
			"    if (req.getStatus() !== 200) {\r\n"
			"        throw response.error;\r\n"
			"    }\r\n"
			"\r\n"
			"    return ''OK'';\r\n"
			"}\r\n"
			"catch (error) {\r\n"
			"    Zabbix.log(3, ''[ DNSViz webhook ] Sending failed. Error: '' + error);\r\n"
			"    throw ''Failed with error: '' + error;\r\n"
			"}");

	DB_EXEC(
			"insert into script_param"
			" set script_paramid=" ZBX_FS_UI64 ",scriptid=" ZBX_FS_UI64 ",name='url'"
				",value='https://dnsviz.net/d/{$RSM.DNS.TESTPREFIX}.{$RSM.TLD}/analyze/'",
			DBget_maxid_num("script_param", 1), scriptid);

	DB_EXEC("delete from ids where table_name='scripts'");
	DB_EXEC("delete from ids where table_name='script_param'");

	ret = SUCCEED;
out:
	return ret;
}

/* 6000000, 24 - add action "Create DNSViz report" for DNSSEC accidents */
static int	DBpatch_6000000_24(void)
{
	zbx_uint64_t	actionid, operationid, scriptid;
	int		ret = FAIL;

	ONLY_SERVER();

	actionid    = DBget_maxid_num("actions", 1);
	operationid = DBget_maxid_num("operations", 1);

	SELECT_VALUE_UINT64(scriptid, "select scriptid from scripts where name='%s'", "DNSViz webhook");

	DB_EXEC(
			"insert into actions"
			" set actionid=" ZBX_FS_UI64 ",name='Create DNSViz report',eventsource=0,evaltype=0,status=1"
				",esc_period='1h',pause_suppressed=1,notify_if_canceled=1",
			actionid);

	DB_EXEC(
			"insert into conditions"
			" set conditionid=" ZBX_FS_UI64 ",actionid=" ZBX_FS_UI64 ",conditiontype=3,operator=2"
			",value='DNSSEC service is down'",
			DBget_maxid_num("conditions", 1), actionid);

	DB_EXEC(
			"insert into operations"
			" set operationid=" ZBX_FS_UI64 ",actionid=" ZBX_FS_UI64 ",operationtype=1"
				",esc_period='0',esc_step_from=1,esc_step_to=1,evaltype=0,recovery=0",
			operationid, actionid);

	DB_EXEC(
			"insert into opcommand"
			" set operationid=" ZBX_FS_UI64 ",scriptid=" ZBX_FS_UI64,
			operationid, scriptid);

	DB_EXEC(
			"insert into opcommand_hst"
			" set opcommand_hstid=" ZBX_FS_UI64 ",operationid=" ZBX_FS_UI64,
			DBget_maxid_num("opcommand_hst", 1), operationid);

	DB_EXEC("delete from ids where table_name='actions'");
	DB_EXEC("delete from ids where table_name='conditions'");
	DB_EXEC("delete from ids where table_name='operations'");
	DB_EXEC("delete from ids where table_name='opcommand'");
	DB_EXEC("delete from ids where table_name='opcommand_hst'");

	ret = SUCCEED;
out:
	return ret;
}

/* 6000000, 25 - create host group "Value maps" */
static int	DBpatch_6000000_25(void)
{
	zbx_uint64_t	groupid;
	int		ret = FAIL;

	ONLY_SERVER();

	groupid = DBget_maxid_num("hstgrp", 1);

	DB_EXEC(
			"insert into hstgrp (groupid,name,internal,flags,uuid)"
			" values (" ZBX_FS_UI64 ",'%s',%d,%d,'%s')",
			groupid, "Value Maps", 0, 0, "5f022f8b797d44c69dbbcecc1e7fcd30");

	DB_EXEC("delete from ids where table_name='hstgrp'");

	ret = SUCCEED;
out:
	return ret;
}

/* 6000000, 26 - add "Value Maps" host group to "Template Value Maps" */
static int	DBpatch_6000000_26(void)
{
	zbx_uint64_t	hostgroupid, hostid, groupid;
	int		ret = FAIL;

	ONLY_SERVER();

	hostgroupid = DBget_maxid_num("hosts_groups", 1);

	SELECT_VALUE_UINT64(groupid, "select groupid from hstgrp where name='%s'", "Value Maps");
	SELECT_VALUE_UINT64(hostid,  "select hostid from hosts where name='%s'", "Template Value Maps");

	DB_EXEC(
			"insert into hosts_groups (hostgroupid,hostid,groupid)"
			" values (" ZBX_FS_UI64 "," ZBX_FS_UI64 "," ZBX_FS_UI64 ")",
			hostgroupid, hostid, groupid);

	DB_EXEC("delete from ids where table_name='hosts_groups'");

	ret = SUCCEED;
out:
	return ret;
}

/* 6000000, 27 - allow "Power user" and "Read-only user" user groups to access "Value Maps" host group */
static int	DBpatch_6000000_27(void)
{
	zbx_uint64_t	rightid, hstgrp_groupid;
	int		ret = FAIL;

	ONLY_SERVER();

	rightid = DBget_maxid_num("rights", 2);

	SELECT_VALUE_UINT64(hstgrp_groupid, "select groupid from hstgrp where name='%s'", "Value Maps");

	DB_EXEC(
			"insert into rights (rightid,groupid,permission,id)"
			" values (" ZBX_FS_UI64 ",%d,%d," ZBX_FS_UI64 ")",
			rightid, 100, 2, hstgrp_groupid);

	DB_EXEC(
			"insert into rights (rightid,groupid,permission,id)"
			" values (" ZBX_FS_UI64 ",%d,%d," ZBX_FS_UI64 ")",
			++rightid, 110, 2, hstgrp_groupid);

	DB_EXEC("delete from ids where table_name='rights'");

	ret = SUCCEED;
out:
	return ret;
}

static int	DBpatch_6000001(void)
{
	if (0 == (program_type & ZBX_PROGRAM_TYPE_SERVER))
		return SUCCEED;

	if (ZBX_DB_OK > DBexecute("delete from profiles where idx='web.auditlog.filter.action' and value_int=-1"))
		return FAIL;

	return SUCCEED;
}

static int	DBpatch_6000002(void)
{
	if (0 == (program_type & ZBX_PROGRAM_TYPE_SERVER))
		return SUCCEED;

	if (ZBX_DB_OK > DBexecute("update profiles set idx='web.auditlog.filter.actions' where"
			" idx='web.auditlog.filter.action'"))
	{
		return FAIL;
	}

	return SUCCEED;
}

#define HTTPSTEP_ITEM_TYPE_RSPCODE	0
#define HTTPSTEP_ITEM_TYPE_TIME		1
#define HTTPSTEP_ITEM_TYPE_IN		2
#define HTTPSTEP_ITEM_TYPE_LASTSTEP	3
#define HTTPSTEP_ITEM_TYPE_LASTERROR	4

static int	DBpatch_6000003(void)
{
	DB_ROW		row;
	DB_RESULT	result;
	int		ret = SUCCEED;
	char		*sql = NULL;
	size_t		sql_alloc = 0, sql_offset = 0, out_alloc = 0;
	char		*out = NULL;

	if (ZBX_PROGRAM_TYPE_SERVER != program_type)
		return SUCCEED;

	DBbegin_multiple_update(&sql, &sql_alloc, &sql_offset);

	result = DBselect(
			"select hi.itemid,hi.type,ht.name"
			" from httptestitem hi,httptest ht"
			" where hi.httptestid=ht.httptestid");

	while (SUCCEED == ret && NULL != (row = DBfetch(result)))
	{
		zbx_uint64_t	itemid;
		char		*esc;
		size_t		out_offset = 0;
		unsigned char	type;

		ZBX_STR2UINT64(itemid, row[0]);
		ZBX_STR2UCHAR(type, row[1]);

		switch (type)
		{
			case HTTPSTEP_ITEM_TYPE_IN:
				zbx_snprintf_alloc(&out, &out_alloc, &out_offset,
						"Download speed for scenario \"%s\".", row[2]);
				break;
			case HTTPSTEP_ITEM_TYPE_LASTSTEP:
				zbx_snprintf_alloc(&out, &out_alloc, &out_offset,
						"Failed step of scenario \"%s\".", row[2]);
				break;
			case HTTPSTEP_ITEM_TYPE_LASTERROR:
				zbx_snprintf_alloc(&out, &out_alloc, &out_offset,
						"Last error message of scenario \"%s\".", row[2]);
				break;
		}
		esc = DBdyn_escape_field("items", "name", out);
		zbx_snprintf_alloc(&sql, &sql_alloc, &sql_offset, "update items set name='%s' where itemid="
				ZBX_FS_UI64 ";\n", esc, itemid);
		zbx_free(esc);

		ret = DBexecute_overflowed_sql(&sql, &sql_alloc, &sql_offset);
	}
	DBfree_result(result);

	DBend_multiple_update(&sql, &sql_alloc, &sql_offset);

	if (SUCCEED == ret && 16 < sql_offset)
	{
		if (ZBX_DB_OK > DBexecute("%s", sql))
			ret = FAIL;
	}

	zbx_free(sql);
	zbx_free(out);

	return ret;
}

static int	DBpatch_6000004(void)
{
	DB_ROW		row;
	DB_RESULT	result;
	int		ret = SUCCEED;
	char		*sql = NULL;
	size_t		sql_alloc = 0, sql_offset = 0, out_alloc = 0;
	char		*out = NULL;

	if (ZBX_PROGRAM_TYPE_SERVER != program_type)
		return SUCCEED;

	DBbegin_multiple_update(&sql, &sql_alloc, &sql_offset);

	result = DBselect(
			"select hi.itemid,hi.type,hs.name,ht.name"
			" from httpstepitem hi,httpstep hs,httptest ht"
			" where hi.httpstepid=hs.httpstepid"
				" and hs.httptestid=ht.httptestid");

	while (SUCCEED == ret && NULL != (row = DBfetch(result)))
	{
		zbx_uint64_t	itemid;
		char		*esc;
		size_t		out_offset = 0;
		unsigned char	type;

		ZBX_STR2UINT64(itemid, row[0]);
		ZBX_STR2UCHAR(type, row[1]);

		switch (type)
		{
			case HTTPSTEP_ITEM_TYPE_IN:
				zbx_snprintf_alloc(&out, &out_alloc, &out_offset,
						"Download speed for step \"%s\" of scenario \"%s\".", row[2], row[3]);
				break;
			case HTTPSTEP_ITEM_TYPE_TIME:
				zbx_snprintf_alloc(&out, &out_alloc, &out_offset,
						"Response time for step \"%s\" of scenario \"%s\".", row[2], row[3]);
				break;
			case HTTPSTEP_ITEM_TYPE_RSPCODE:
				zbx_snprintf_alloc(&out, &out_alloc, &out_offset,
						"Response code for step \"%s\" of scenario \"%s\".", row[2], row[3]);
				break;
		}

		esc = DBdyn_escape_field("items", "name", out);
		zbx_snprintf_alloc(&sql, &sql_alloc, &sql_offset, "update items set name='%s' where itemid="
				ZBX_FS_UI64 ";\n", esc, itemid);
		zbx_free(esc);

		ret = DBexecute_overflowed_sql(&sql, &sql_alloc, &sql_offset);
	}
	DBfree_result(result);

	DBend_multiple_update(&sql, &sql_alloc, &sql_offset);

	if (SUCCEED == ret && 16 < sql_offset)
	{
		if (ZBX_DB_OK > DBexecute("%s", sql))
			ret = FAIL;
	}

	zbx_free(sql);
	zbx_free(out);

	return ret;
}

#undef HTTPSTEP_ITEM_TYPE_RSPCODE
#undef HTTPSTEP_ITEM_TYPE_TIME
#undef HTTPSTEP_ITEM_TYPE_IN
#undef HTTPSTEP_ITEM_TYPE_LASTSTEP
#undef HTTPSTEP_ITEM_TYPE_LASTERROR

/* 6000004, 1 - add macros {$RDAP.TEST.DOMAIN.DYNAMIC}, {$RDAP.RDAPOUTPUT.DIRECTORY} as item parameters and fix some macro descriptions */
static int	DBpatch_6000004_1(void)
{
	int	ret = FAIL;

	/* table, column, from, to, where */
	char *lines[][5] = {
		{"items", "key_", "{$RDAP.TEST.DOMAIN}", "{$RDAP.TEST.DOMAIN},{$RDAP.TEST.DOMAIN.DYNAMIC}", "rdap[%]"},
		{"items", "key_", "{$RSM.RESOLVER}", "{$RSM.RESOLVER},{$RDAP.RDAPOUTPUT.DIRECTORY}", "rdap[%]"},
		{"globalmacro", "description", "rsm.rdds[{$RSM.TLD},,]", "rsm.rdds[...]", "%rsm.rdds[{$RSM.TLD},,]%"},
		{"globalmacro", "description", "resolver.status[{$RSM.RESOLVER},{$RESOLVER.STATUS.TIMEOUT},{$RESOLVER.STATUS.TRIES},{$RSM.IP4.ENABLED},{$RSM.IP6.ENABLED}]", "resolver.status[...]", "%resolver.status[%"},
		{"globalmacro", "description", "rdap[{$RSM.TLD},{$RDAP.TEST.DOMAIN},{$RDAP.BASE.URL},{$RSM.RDDS.MAXREDIRS},{$RSM.RDDS.RTT.HIGH},{$RDAP.TLD.ENABLED},{$RSM.RDAP.ENABLED},{$RSM.IP4.ENABLED},{$RSM.IP6.ENABLED},{$RSM.RESOLVER}]", "rdap[...]", "%rdap[%"},
		{NULL, NULL, NULL, NULL, NULL}
	};

	ONLY_SERVER();

	for (int i = 0; lines[i][0] != NULL; i++)
	{
		DB_EXEC("update %s set %s=replace(%s,'%s','%s') where %s like '%s'",
				lines[i][0],
				lines[i][1],
				lines[i][1],
				lines[i][2],
				lines[i][3],
				lines[i][1],
				lines[i][4]);
	}

	DB_EXEC(
			"insert into globalmacro"
			" set globalmacroid=" ZBX_FS_UI64
				",macro='%s'"
				",value='%s'"
				",description='%s'"
				",type=0",
			DBget_maxid_num("globalmacro", 1),
			"{$RDAP.RDAPOUTPUT.DIRECTORY}",
			"",
			"Directory for storing RDAP test output files. Leave empty for not storing them at all.");

	ret = SUCCEED;
out:
	return ret;
}

/* 6000004, 2 - add macro {$RDAP.TEST.DOMAIN.DYNAMIC} to each Rsmhost template */
static int	DBpatch_6000004_2(void)
{
	int		ret = FAIL;

	DB_RESULT	result = NULL;
	DB_ROW		row;

	ONLY_SERVER();

	result = DBselect("select hostid from hosts where host like 'Template Rsmhost Config %%'");

	if (NULL == result)
		goto out;

	while (NULL != (row = DBfetch(result)))
	{
		zbx_uint64_t	hostid;

		ZBX_STR2UINT64(hostid, row[0]);

#define SQL	"insert into hostmacro set hostmacroid=" ZBX_FS_UI64 ",hostid=" ZBX_FS_UI64 ",macro='%s',value='%s',description='%s',type=%d"
		DB_EXEC(SQL, DBget_maxid_num("hostmacro", 1), hostid, "{$RDAP.TEST.DOMAIN.DYNAMIC}", "", "Test domains for RDAP queries, this macro is created with empty value when Rsmhost is added. Afterwards it is managed by macro updating script.", 0);
#undef SQL
	}

	ret = SUCCEED;
out:
	DBfree_result(result);

	return ret;
}

#endif

/* 6000004, 3 - change DNSViz action from Webhook to Script */
static int	DBpatch_6000004_3(void)
{
	ONLY_SERVER();

	if (ZBX_DB_OK > DBexecute(
			"update scripts"
			" set name='DNSViz update TLD',execute_on=1,type=0,timeout='30s'"
				",command='/opt/zabbix/scripts/dnsviz-update-tld.sh {$RSM.DNS.TESTPREFIX}.{$RSM.TLD} &'"
			" where scriptid=4"))
	{
		return FAIL;
	}

	return SUCCEED;
}

#ifndef HAVE_SQLITE3
/* 6000004, 4 - update service triggers to have manual close */
static int	DBpatch_6000004_4(void)
{
	ONLY_SERVER();

	if (ZBX_DB_OK > DBexecute(
			"update triggers"
			" set manual_close=1"
			" where description in ("
				"'DNS service is down',"
				"'DNSSEC service is down',"
				"'RDAP service is down',"
				"'RDDS service is down')"))
	{
		return FAIL;
	}

	return SUCCEED;
}
#endif

/* 6000004, 5 - fix sort order of the value map entries */
static int	DBpatch_6000004_5(void)
{
	ONLY_SERVER();

	/* this is just a direct paste from data.tmpl, with each line quoted and properly indented */
	static const char	*const data[] = {
		"ROW   |13                |6         |-1   |Internal error                                                                                          |0   |0        |",
		"ROW   |14                |6         |-200 |DNS UDP - No reply from name server                                                                     |0   |3        |",
		"ROW   |15                |6         |-201 |Invalid reply from Name Server (obsolete)                                                               |0   |4        |",
		"ROW   |16                |6         |-202 |No UNIX timestamp (obsolete)                                                                            |0   |5        |",
		"ROW   |17                |6         |-203 |Invalid UNIX timestamp (obsolete)                                                                       |0   |6        |",
		"ROW   |18                |6         |-204 |DNSSEC error (obsolete)                                                                                 |0   |7        |",
		"ROW   |19                |6         |-205 |No reply from resolver (obsolete)                                                                       |0   |8        |",
		"ROW   |20                |6         |-206 |Keyset is not valid (obsolete)                                                                          |0   |9        |",
		"ROW   |21                |6         |-207 |DNS UDP - Expecting DNS CLASS IN but got CHAOS                                                          |0   |10       |",
		"ROW   |22                |6         |-208 |DNS UDP - Expecting DNS CLASS IN but got HESIOD                                                         |0   |11       |",
		"ROW   |23                |6         |-209 |DNS UDP - Expecting DNS CLASS IN but got something different than IN, CHAOS or HESIOD                   |0   |12       |",
		"ROW   |24                |6         |-210 |DNS UDP - Header section incomplete                                                                     |0   |13       |",
		"ROW   |25                |6         |-211 |DNS UDP - Question section incomplete                                                                   |0   |14       |",
		"ROW   |26                |6         |-212 |DNS UDP - Answer section incomplete                                                                     |0   |15       |",
		"ROW   |27                |6         |-213 |DNS UDP - Authority section incomplete                                                                  |0   |16       |",
		"ROW   |28                |6         |-214 |DNS UDP - Additional section incomplete                                                                 |0   |17       |",
		"ROW   |29                |6         |-215 |DNS UDP - Malformed DNS response                                                                        |0   |18       |",
		"ROW   |30                |6         |-250 |DNS UDP - Querying for a non existent domain - AA flag not present in response                          |0   |19       |",
		"ROW   |31                |6         |-251 |DNS UDP - Querying for a non existent domain - Domain name being queried not present in question section|0   |20       |",
		"ROW   |32                |6         |-252 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got NOERROR (obsolete)      |0   |21       |",
		"ROW   |33                |6         |-253 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got FORMERR                 |0   |22       |",
		"ROW   |34                |6         |-254 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got SERVFAIL                |0   |23       |",
		"ROW   |35                |6         |-255 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got NOTIMP                  |0   |24       |",
		"ROW   |36                |6         |-256 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got REFUSED                 |0   |25       |",
		"ROW   |37                |6         |-257 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got YXDOMAIN                |0   |26       |",
		"ROW   |38                |6         |-258 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got YXRRSET                 |0   |27       |",
		"ROW   |39                |6         |-259 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got NXRRSET                 |0   |28       |",
		"ROW   |40                |6         |-260 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got NOTAUTH                 |0   |29       |",
		"ROW   |41                |6         |-261 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got NOTZONE                 |0   |30       |",
		"ROW   |42                |6         |-262 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADVERS or BADSIG       |0   |31       |",
		"ROW   |43                |6         |-263 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADKEY                  |0   |32       |",
		"ROW   |44                |6         |-264 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADTIME                 |0   |33       |",
		"ROW   |45                |6         |-265 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADMODE                 |0   |34       |",
		"ROW   |46                |6         |-266 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADNAME                 |0   |35       |",
		"ROW   |47                |6         |-267 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADALG                  |0   |36       |",
		"ROW   |48                |6         |-268 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADTRUNC                |0   |37       |",
		"ROW   |49                |6         |-269 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADCOOKIE               |0   |38       |",
		"ROW   |50                |6         |-270 |DNS UDP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got unexpected              |0   |39       |",
		"ROW   |51                |6         |-400 |DNS UDP - No server could be reached by local resolver                                                  |0   |40       |",
		"ROW   |52                |6         |-401 |DNS UDP - The TLD is configured as DNSSEC-enabled, but no DNSKEY was found in the apex                  |0   |41       |",
		"ROW   |53                |6         |-402 |DNS UDP - No AD bit from local resolver                                                                 |0   |42       |",
		"ROW   |54                |6         |-403 |DNS UDP - Expecting NOERROR RCODE but got NXDOMAIN from local resolver                                  |0   |43       |",
		"ROW   |55                |6         |-2   |DNS UDP - Expecting NOERROR RCODE but got unexpected from local resolver                                |0   |1        |",
		"ROW   |56                |6         |-405 |DNS UDP - Unknown cryptographic algorithm                                                               |0   |44       |",
		"ROW   |57                |6         |-406 |DNS UDP - Cryptographic algorithm not implemented                                                       |0   |45       |",
		"ROW   |58                |6         |-407 |DNS UDP - No RRSIGs where found in any section, and the TLD has the DNSSEC flag enabled                 |0   |46       |",
		"ROW   |59                |6         |-410 |DNS UDP - The signature does not cover this RRset                                                       |0   |48       |",
		"ROW   |60                |6         |-414 |DNS UDP - The RRSIG found is not signed by a DNSKEY from the KEYSET of the TLD                          |0   |49       |",
		"ROW   |61                |6         |-415 |DNS UDP - Bogus DNSSEC signature                                                                        |0   |50       |",
		"ROW   |62                |6         |-416 |DNS UDP - DNSSEC signature has expired                                                                  |0   |51       |",
		"ROW   |63                |6         |-417 |DNS UDP - DNSSEC signature not incepted yet                                                             |0   |52       |",
		"ROW   |64                |6         |-418 |DNS UDP - DNSSEC signature has expiration date earlier than inception date                              |0   |53       |",
		"ROW   |65                |6         |-419 |DNS UDP - Error in NSEC3 denial of existence proof                                                      |0   |54       |",
		"ROW   |66                |6         |-408 |DNS UDP - Querying for a non existent domain - No NSEC/NSEC3 RRs were found in the authority section    |0   |47       |",
		"ROW   |67                |6         |-422 |DNS UDP - RR not covered by the given NSEC RRs                                                          |0   |55       |",
		"ROW   |68                |6         |-423 |DNS UDP - Wildcard not covered by the given NSEC RRs                                                    |0   |56       |",
		"ROW   |69                |6         |-425 |DNS UDP - The RRSIG has too few RDATA fields                                                            |0   |57       |",
		"ROW   |70                |6         |-427 |DNS UDP - Malformed DNSSEC response                                                                     |0   |58       |",
		"ROW   |71                |6         |-600 |DNS TCP - Timeout reply from name server                                                                |0   |59       |",
		"ROW   |72                |6         |-601 |DNS TCP - Error opening connection to name server                                                       |0   |60       |",
		"ROW   |73                |6         |-607 |DNS TCP - Expecting DNS CLASS IN but got CHAOS                                                          |0   |61       |",
		"ROW   |74                |6         |-608 |DNS TCP - Expecting DNS CLASS IN but got HESIOD                                                         |0   |62       |",
		"ROW   |75                |6         |-609 |DNS TCP - Expecting DNS CLASS IN but got something different than IN, CHAOS or HESIOD                   |0   |63       |",
		"ROW   |76                |6         |-610 |DNS TCP - Header section incomplete                                                                     |0   |64       |",
		"ROW   |77                |6         |-611 |DNS TCP - Question section incomplete                                                                   |0   |65       |",
		"ROW   |78                |6         |-612 |DNS TCP - Answer section incomplete                                                                     |0   |66       |",
		"ROW   |79                |6         |-613 |DNS TCP - Authority section incomplete                                                                  |0   |67       |",
		"ROW   |80                |6         |-614 |DNS TCP - Additional section incomplete                                                                 |0   |68       |",
		"ROW   |81                |6         |-615 |DNS TCP - Malformed DNS response                                                                        |0   |69       |",
		"ROW   |82                |6         |-650 |DNS TCP - Querying for a non existent domain - AA flag not present in response                          |0   |70       |",
		"ROW   |83                |6         |-651 |DNS TCP - Querying for a non existent domain - Domain name being queried not present in question section|0   |71       |",
		"ROW   |84                |6         |-652 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got NOERROR (obsolete)      |0   |72       |",
		"ROW   |85                |6         |-653 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got FORMERR                 |0   |73       |",
		"ROW   |86                |6         |-654 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got SERVFAIL                |0   |74       |",
		"ROW   |87                |6         |-655 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got NOTIMP                  |0   |75       |",
		"ROW   |88                |6         |-656 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got REFUSED                 |0   |76       |",
		"ROW   |89                |6         |-657 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got YXDOMAIN                |0   |77       |",
		"ROW   |90                |6         |-658 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got YXRRSET                 |0   |78       |",
		"ROW   |91                |6         |-659 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got NXRRSET                 |0   |79       |",
		"ROW   |92                |6         |-660 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got NOTAUTH                 |0   |80       |",
		"ROW   |93                |6         |-661 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got NOTZONE                 |0   |81       |",
		"ROW   |94                |6         |-662 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADVERS or BADSIG       |0   |82       |",
		"ROW   |95                |6         |-663 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADKEY                  |0   |83       |",
		"ROW   |96                |6         |-664 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADTIME                 |0   |84       |",
		"ROW   |97                |6         |-665 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADMODE                 |0   |85       |",
		"ROW   |98                |6         |-666 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADNAME                 |0   |86       |",
		"ROW   |99                |6         |-667 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADALG                  |0   |87       |",
		"ROW   |100               |6         |-668 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADTRUNC                |0   |88       |",
		"ROW   |101               |6         |-669 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got BADCOOKIE               |0   |89       |",
		"ROW   |102               |6         |-670 |DNS TCP - Querying for a non existent domain - Expecting NXDOMAIN RCODE but got unexpected              |0   |90       |",
		"ROW   |103               |6         |-800 |DNS TCP - No server could be reached by local resolver                                                  |0   |91       |",
		"ROW   |104               |6         |-801 |DNS TCP - The TLD is configured as DNSSEC-enabled, but no DNSKEY was found in the apex                  |0   |92       |",
		"ROW   |105               |6         |-802 |DNS TCP - No AD bit from local resolver                                                                 |0   |93       |",
		"ROW   |106               |6         |-803 |DNS TCP - Expecting NOERROR RCODE but got NXDOMAIN from local resolver                                  |0   |94       |",
		"ROW   |107               |6         |-3   |DNS TCP - Expecting NOERROR RCODE but got unexpected from local resolver                                |0   |2        |",
		"ROW   |108               |6         |-805 |DNS TCP - Unknown cryptographic algorithm                                                               |0   |95       |",
		"ROW   |109               |6         |-806 |DNS TCP - Cryptographic algorithm not implemented                                                       |0   |96       |",
		"ROW   |110               |6         |-807 |DNS TCP - No RRSIGs where found in any section, and the TLD has the DNSSEC flag enabled                 |0   |97       |",
		"ROW   |111               |6         |-810 |DNS TCP - The signature does not cover this RRset                                                       |0   |99       |",
		"ROW   |112               |6         |-814 |DNS TCP - The RRSIG found is not signed by a DNSKEY from the KEYSET of the TLD                          |0   |100      |",
		"ROW   |113               |6         |-815 |DNS TCP - Bogus DNSSEC signature                                                                        |0   |101      |",
		"ROW   |114               |6         |-816 |DNS TCP - DNSSEC signature has expired                                                                  |0   |102      |",
		"ROW   |115               |6         |-817 |DNS TCP - DNSSEC signature not incepted yet                                                             |0   |103      |",
		"ROW   |116               |6         |-818 |DNS TCP - DNSSEC signature has expiration date earlier than inception date                              |0   |104      |",
		"ROW   |117               |6         |-819 |DNS TCP - Error in NSEC3 denial of existence proof                                                      |0   |105      |",
		"ROW   |118               |6         |-808 |DNS TCP - Querying for a non existent domain - No NSEC/NSEC3 RRs were found in the authority section    |0   |98       |",
		"ROW   |119               |6         |-822 |DNS TCP - RR not covered by the given NSEC RRs                                                          |0   |106      |",
		"ROW   |120               |6         |-823 |DNS TCP - Wildcard not covered by the given NSEC RRs                                                    |0   |107      |",
		"ROW   |121               |6         |-825 |DNS TCP - The RRSIG has too few RDATA fields                                                            |0   |108      |",
		"ROW   |122               |6         |-827 |DNS TCP - Malformed DNSSEC response                                                                     |0   |109      |",
		"ROW   |123               |7         |-1   |Internal error                                                                                          |0   |0        |",
		"ROW   |124               |7         |-200 |No reply from RDDS43 server (obsolete)                                                                  |0   |4        |",
		"ROW   |125               |7         |-201 |Whois server returned no NS                                                                             |0   |5        |",
		"ROW   |126               |7         |-202 |No Unix timestamp (obsolete)                                                                            |0   |6        |",
		"ROW   |127               |7         |-203 |Invalid Unix timestamp (obsolete)                                                                       |0   |7        |",
		"ROW   |128               |7         |-204 |No reply from RDDS80 server (obsolete)                                                                  |0   |8        |",
		"ROW   |129               |7         |-205 |Cannot resolve a Whois host name (obsolete)                                                             |0   |9        |",
		"ROW   |130               |7         |-206 |no HTTP status code                                                                                     |0   |10       |",
		"ROW   |131               |7         |-207 |invalid HTTP status code (obsolete)                                                                     |0   |11       |",
		"ROW   |132               |7         |-222 |RDDS43 - No server could be reached by local resolver                                                   |0   |12       |",
		"ROW   |133               |7         |-224 |RDDS43 - Expecting NOERROR RCODE but got SERVFAIL when resolving hostname                               |0   |13       |",
		"ROW   |134               |7         |-225 |RDDS43 - Expecting NOERROR RCODE but got NXDOMAIN when resolving hostname                               |0   |14       |",
		"ROW   |135               |7         |-3   |RDDS43 - Expecting NOERROR RCODE but got unexpected error when resolving hostname                       |0   |2        |",
		"ROW   |136               |7         |-227 |RDDS43 - Timeout                                                                                        |0   |16       |",
		"ROW   |137               |7         |-228 |RDDS43 - Error opening connection to server                                                             |0   |17       |",
		"ROW   |138               |7         |-229 |RDDS43 - Empty response                                                                                 |0   |18       |",
		"ROW   |139               |7         |-250 |RDDS80 - No server could be reached by local resolver                                                   |0   |19       |",
		"ROW   |140               |7         |-2   |RDDS - IP addresses for the hostname are not supported by the IP versions supported by the probe node   |0   |1        |",
		"ROW   |141               |7         |-252 |RDDS80 - Expecting NOERROR RCODE but got SERVFAIL when resolving hostname                               |0   |20       |",
		"ROW   |142               |7         |-253 |RDDS80 - Expecting NOERROR RCODE but got NXDOMAIN when resolving hostname                               |0   |21       |",
		"ROW   |143               |7         |-4   |RDDS80 - Expecting NOERROR RCODE but got unexpected error when resolving hostname                       |0   |3        |",
		"ROW   |144               |7         |-255 |RDDS80 - Timeout                                                                                        |0   |23       |",
		"ROW   |145               |7         |-256 |RDDS80 - Error opening connection to server                                                             |0   |24       |",
		"ROW   |146               |7         |-257 |RDDS80 - Error in HTTP protocol                                                                         |0   |25       |",
		"ROW   |147               |7         |-258 |RDDS80 - Error in HTTPS protocol                                                                        |0   |26       |",
		"ROW   |148               |7         |-300 |RDDS80 - Expecting HTTP status code 200 but got 100                                                     |0   |28       |",
		"ROW   |149               |7         |-301 |RDDS80 - Expecting HTTP status code 200 but got 101                                                     |0   |29       |",
		"ROW   |150               |7         |-302 |RDDS80 - Expecting HTTP status code 200 but got 102                                                     |0   |30       |",
		"ROW   |151               |7         |-303 |RDDS80 - Expecting HTTP status code 200 but got 103                                                     |0   |31       |",
		"ROW   |152               |7         |-304 |RDDS80 - Expecting HTTP status code 200 but got 201                                                     |0   |32       |",
		"ROW   |153               |7         |-305 |RDDS80 - Expecting HTTP status code 200 but got 202                                                     |0   |33       |",
		"ROW   |154               |7         |-306 |RDDS80 - Expecting HTTP status code 200 but got 203                                                     |0   |34       |",
		"ROW   |155               |7         |-307 |RDDS80 - Expecting HTTP status code 200 but got 204                                                     |0   |35       |",
		"ROW   |156               |7         |-308 |RDDS80 - Expecting HTTP status code 200 but got 205                                                     |0   |36       |",
		"ROW   |157               |7         |-309 |RDDS80 - Expecting HTTP status code 200 but got 206                                                     |0   |37       |",
		"ROW   |158               |7         |-310 |RDDS80 - Expecting HTTP status code 200 but got 207                                                     |0   |38       |",
		"ROW   |159               |7         |-311 |RDDS80 - Expecting HTTP status code 200 but got 208                                                     |0   |39       |",
		"ROW   |160               |7         |-312 |RDDS80 - Expecting HTTP status code 200 but got 226                                                     |0   |40       |",
		"ROW   |161               |7         |-313 |RDDS80 - Expecting HTTP status code 200 but got 300                                                     |0   |41       |",
		"ROW   |162               |7         |-317 |RDDS80 - Expecting HTTP status code 200 but got 304                                                     |0   |42       |",
		"ROW   |163               |7         |-318 |RDDS80 - Expecting HTTP status code 200 but got 305                                                     |0   |43       |",
		"ROW   |164               |7         |-319 |RDDS80 - Expecting HTTP status code 200 but got 306                                                     |0   |44       |",
		"ROW   |165               |7         |-320 |RDDS80 - Expecting HTTP status code 200 but got 307                                                     |0   |45       |",
		"ROW   |166               |7         |-321 |RDDS80 - Expecting HTTP status code 200 but got 308                                                     |0   |46       |",
		"ROW   |167               |7         |-322 |RDDS80 - Expecting HTTP status code 200 but got 400                                                     |0   |47       |",
		"ROW   |168               |7         |-323 |RDDS80 - Expecting HTTP status code 200 but got 401                                                     |0   |48       |",
		"ROW   |169               |7         |-324 |RDDS80 - Expecting HTTP status code 200 but got 402                                                     |0   |49       |",
		"ROW   |170               |7         |-325 |RDDS80 - Expecting HTTP status code 200 but got 403                                                     |0   |50       |",
		"ROW   |171               |7         |-326 |RDDS80 - Expecting HTTP status code 200 but got 404                                                     |0   |51       |",
		"ROW   |172               |7         |-327 |RDDS80 - Expecting HTTP status code 200 but got 405                                                     |0   |52       |",
		"ROW   |173               |7         |-328 |RDDS80 - Expecting HTTP status code 200 but got 406                                                     |0   |53       |",
		"ROW   |174               |7         |-329 |RDDS80 - Expecting HTTP status code 200 but got 407                                                     |0   |54       |",
		"ROW   |175               |7         |-330 |RDDS80 - Expecting HTTP status code 200 but got 408                                                     |0   |55       |",
		"ROW   |176               |7         |-331 |RDDS80 - Expecting HTTP status code 200 but got 409                                                     |0   |56       |",
		"ROW   |177               |7         |-332 |RDDS80 - Expecting HTTP status code 200 but got 410                                                     |0   |57       |",
		"ROW   |178               |7         |-333 |RDDS80 - Expecting HTTP status code 200 but got 411                                                     |0   |58       |",
		"ROW   |179               |7         |-334 |RDDS80 - Expecting HTTP status code 200 but got 412                                                     |0   |59       |",
		"ROW   |180               |7         |-335 |RDDS80 - Expecting HTTP status code 200 but got 413                                                     |0   |60       |",
		"ROW   |181               |7         |-336 |RDDS80 - Expecting HTTP status code 200 but got 414                                                     |0   |61       |",
		"ROW   |182               |7         |-337 |RDDS80 - Expecting HTTP status code 200 but got 415                                                     |0   |62       |",
		"ROW   |183               |7         |-338 |RDDS80 - Expecting HTTP status code 200 but got 416                                                     |0   |63       |",
		"ROW   |184               |7         |-339 |RDDS80 - Expecting HTTP status code 200 but got 417                                                     |0   |64       |",
		"ROW   |185               |7         |-340 |RDDS80 - Expecting HTTP status code 200 but got 421                                                     |0   |65       |",
		"ROW   |186               |7         |-341 |RDDS80 - Expecting HTTP status code 200 but got 422                                                     |0   |66       |",
		"ROW   |187               |7         |-342 |RDDS80 - Expecting HTTP status code 200 but got 423                                                     |0   |67       |",
		"ROW   |188               |7         |-343 |RDDS80 - Expecting HTTP status code 200 but got 424                                                     |0   |68       |",
		"ROW   |189               |7         |-344 |RDDS80 - Expecting HTTP status code 200 but got 426                                                     |0   |69       |",
		"ROW   |190               |7         |-345 |RDDS80 - Expecting HTTP status code 200 but got 428                                                     |0   |70       |",
		"ROW   |191               |7         |-346 |RDDS80 - Expecting HTTP status code 200 but got 429                                                     |0   |71       |",
		"ROW   |192               |7         |-347 |RDDS80 - Expecting HTTP status code 200 but got 431                                                     |0   |72       |",
		"ROW   |193               |7         |-348 |RDDS80 - Expecting HTTP status code 200 but got 451                                                     |0   |73       |",
		"ROW   |194               |7         |-349 |RDDS80 - Expecting HTTP status code 200 but got 500                                                     |0   |74       |",
		"ROW   |195               |7         |-350 |RDDS80 - Expecting HTTP status code 200 but got 501                                                     |0   |75       |",
		"ROW   |196               |7         |-351 |RDDS80 - Expecting HTTP status code 200 but got 502                                                     |0   |76       |",
		"ROW   |197               |7         |-352 |RDDS80 - Expecting HTTP status code 200 but got 503                                                     |0   |77       |",
		"ROW   |198               |7         |-353 |RDDS80 - Expecting HTTP status code 200 but got 504                                                     |0   |78       |",
		"ROW   |199               |7         |-354 |RDDS80 - Expecting HTTP status code 200 but got 505                                                     |0   |79       |",
		"ROW   |200               |7         |-355 |RDDS80 - Expecting HTTP status code 200 but got 506                                                     |0   |80       |",
		"ROW   |201               |7         |-356 |RDDS80 - Expecting HTTP status code 200 but got 507                                                     |0   |81       |",
		"ROW   |202               |7         |-357 |RDDS80 - Expecting HTTP status code 200 but got 508                                                     |0   |82       |",
		"ROW   |203               |7         |-358 |RDDS80 - Expecting HTTP status code 200 but got 510                                                     |0   |83       |",
		"ROW   |204               |7         |-359 |RDDS80 - Expecting HTTP status code 200 but got 511                                                     |0   |84       |",
		"ROW   |205               |7         |-360 |RDDS80 - Expecting HTTP status code 200 but got unexpected status code                                  |0   |85       |",
		"ROW   |206               |7         |-259 |RDDS80 - Maximum HTTP redirects were hit while trying to connect to RDDS server                         |0   |27       |",
		"ROW   |207               |8         |-1   |Internal error                                                                                          |0   |0        |",
		"ROW   |208               |8         |-390 |The TLD is not listed in the Bootstrap Service Registry for Domain Name Space                           |0   |3        |",
		"ROW   |209               |8         |-391 |The RDAP base URL obtained from Bootstrap Service Registry for Domain Name Space does not use HTTPS     |0   |4        |",
		"ROW   |210               |8         |-400 |RDAP - No server could be reached by local resolver                                                     |0   |5        |",
		"ROW   |211               |8         |-402 |RDAP - Expecting NOERROR RCODE but got SERVFAIL when resolving hostname                                 |0   |6        |",
		"ROW   |212               |8         |-403 |RDAP - Expecting NOERROR RCODE but got NXDOMAIN when resolving hostname                                 |0   |7        |",
		"ROW   |213               |8         |-5   |RDAP - Expecting NOERROR RCODE but got unexpected error when resolving hostname                         |0   |2        |",
		"ROW   |214               |8         |-405 |RDAP - Timeout                                                                                          |0   |9        |",
		"ROW   |215               |8         |-406 |RDAP - Error opening connection to server                                                               |0   |10       |",
		"ROW   |216               |8         |-407 |RDAP - Invalid JSON format in response                                                                  |0   |11       |",
		"ROW   |217               |8         |-408 |RDAP - ldhName member not found in response                                                             |0   |12       |",
		"ROW   |218               |8         |-409 |RDAP - ldhName member doesn't match query in response                                                   |0   |13       |",
		"ROW   |219               |8         |-413 |RDAP - Error in HTTP protocol                                                                           |0   |14       |",
		"ROW   |220               |8         |-414 |RDAP - Error in HTTPS protocol                                                                          |0   |15       |",
		"ROW   |221               |8         |-500 |RDAP - Expecting HTTP status code 200 but got 100                                                       |0   |17       |",
		"ROW   |222               |8         |-501 |RDAP - Expecting HTTP status code 200 but got 101                                                       |0   |18       |",
		"ROW   |223               |8         |-502 |RDAP - Expecting HTTP status code 200 but got 102                                                       |0   |19       |",
		"ROW   |224               |8         |-503 |RDAP - Expecting HTTP status code 200 but got 103                                                       |0   |20       |",
		"ROW   |225               |8         |-504 |RDAP - Expecting HTTP status code 200 but got 201                                                       |0   |21       |",
		"ROW   |226               |8         |-505 |RDAP - Expecting HTTP status code 200 but got 202                                                       |0   |22       |",
		"ROW   |227               |8         |-506 |RDAP - Expecting HTTP status code 200 but got 203                                                       |0   |23       |",
		"ROW   |228               |8         |-507 |RDAP - Expecting HTTP status code 200 but got 204                                                       |0   |24       |",
		"ROW   |229               |8         |-508 |RDAP - Expecting HTTP status code 200 but got 205                                                       |0   |25       |",
		"ROW   |230               |8         |-509 |RDAP - Expecting HTTP status code 200 but got 206                                                       |0   |26       |",
		"ROW   |231               |8         |-510 |RDAP - Expecting HTTP status code 200 but got 207                                                       |0   |27       |",
		"ROW   |232               |8         |-511 |RDAP - Expecting HTTP status code 200 but got 208                                                       |0   |28       |",
		"ROW   |233               |8         |-512 |RDAP - Expecting HTTP status code 200 but got 226                                                       |0   |29       |",
		"ROW   |234               |8         |-513 |RDAP - Expecting HTTP status code 200 but got 300                                                       |0   |30       |",
		"ROW   |235               |8         |-517 |RDAP - Expecting HTTP status code 200 but got 304                                                       |0   |31       |",
		"ROW   |236               |8         |-518 |RDAP - Expecting HTTP status code 200 but got 305                                                       |0   |32       |",
		"ROW   |237               |8         |-519 |RDAP - Expecting HTTP status code 200 but got 306                                                       |0   |33       |",
		"ROW   |238               |8         |-520 |RDAP - Expecting HTTP status code 200 but got 307                                                       |0   |34       |",
		"ROW   |239               |8         |-521 |RDAP - Expecting HTTP status code 200 but got 308                                                       |0   |35       |",
		"ROW   |240               |8         |-522 |RDAP - Expecting HTTP status code 200 but got 400                                                       |0   |36       |",
		"ROW   |241               |8         |-523 |RDAP - Expecting HTTP status code 200 but got 401                                                       |0   |37       |",
		"ROW   |242               |8         |-524 |RDAP - Expecting HTTP status code 200 but got 402                                                       |0   |38       |",
		"ROW   |243               |8         |-525 |RDAP - Expecting HTTP status code 200 but got 403                                                       |0   |39       |",
		"ROW   |244               |8         |-526 |RDAP - Expecting HTTP status code 200 but got 404                                                       |0   |40       |",
		"ROW   |245               |8         |-527 |RDAP - Expecting HTTP status code 200 but got 405                                                       |0   |41       |",
		"ROW   |246               |8         |-528 |RDAP - Expecting HTTP status code 200 but got 406                                                       |0   |42       |",
		"ROW   |247               |8         |-529 |RDAP - Expecting HTTP status code 200 but got 407                                                       |0   |43       |",
		"ROW   |248               |8         |-530 |RDAP - Expecting HTTP status code 200 but got 408                                                       |0   |44       |",
		"ROW   |249               |8         |-531 |RDAP - Expecting HTTP status code 200 but got 409                                                       |0   |45       |",
		"ROW   |250               |8         |-532 |RDAP - Expecting HTTP status code 200 but got 410                                                       |0   |46       |",
		"ROW   |251               |8         |-533 |RDAP - Expecting HTTP status code 200 but got 411                                                       |0   |47       |",
		"ROW   |252               |8         |-534 |RDAP - Expecting HTTP status code 200 but got 412                                                       |0   |48       |",
		"ROW   |253               |8         |-535 |RDAP - Expecting HTTP status code 200 but got 413                                                       |0   |49       |",
		"ROW   |254               |8         |-536 |RDAP - Expecting HTTP status code 200 but got 414                                                       |0   |50       |",
		"ROW   |255               |8         |-537 |RDAP - Expecting HTTP status code 200 but got 415                                                       |0   |51       |",
		"ROW   |256               |8         |-538 |RDAP - Expecting HTTP status code 200 but got 416                                                       |0   |52       |",
		"ROW   |257               |8         |-539 |RDAP - Expecting HTTP status code 200 but got 417                                                       |0   |53       |",
		"ROW   |258               |8         |-540 |RDAP - Expecting HTTP status code 200 but got 421                                                       |0   |54       |",
		"ROW   |259               |8         |-541 |RDAP - Expecting HTTP status code 200 but got 422                                                       |0   |55       |",
		"ROW   |260               |8         |-542 |RDAP - Expecting HTTP status code 200 but got 423                                                       |0   |56       |",
		"ROW   |261               |8         |-543 |RDAP - Expecting HTTP status code 200 but got 424                                                       |0   |57       |",
		"ROW   |262               |8         |-544 |RDAP - Expecting HTTP status code 200 but got 426                                                       |0   |58       |",
		"ROW   |263               |8         |-545 |RDAP - Expecting HTTP status code 200 but got 428                                                       |0   |59       |",
		"ROW   |264               |8         |-546 |RDAP - Expecting HTTP status code 200 but got 429                                                       |0   |60       |",
		"ROW   |265               |8         |-547 |RDAP - Expecting HTTP status code 200 but got 431                                                       |0   |61       |",
		"ROW   |266               |8         |-548 |RDAP - Expecting HTTP status code 200 but got 451                                                       |0   |62       |",
		"ROW   |267               |8         |-549 |RDAP - Expecting HTTP status code 200 but got 500                                                       |0   |63       |",
		"ROW   |268               |8         |-550 |RDAP - Expecting HTTP status code 200 but got 501                                                       |0   |64       |",
		"ROW   |269               |8         |-551 |RDAP - Expecting HTTP status code 200 but got 502                                                       |0   |65       |",
		"ROW   |270               |8         |-552 |RDAP - Expecting HTTP status code 200 but got 503                                                       |0   |66       |",
		"ROW   |271               |8         |-553 |RDAP - Expecting HTTP status code 200 but got 504                                                       |0   |67       |",
		"ROW   |272               |8         |-554 |RDAP - Expecting HTTP status code 200 but got 505                                                       |0   |68       |",
		"ROW   |273               |8         |-555 |RDAP - Expecting HTTP status code 200 but got 506                                                       |0   |69       |",
		"ROW   |274               |8         |-556 |RDAP - Expecting HTTP status code 200 but got 507                                                       |0   |70       |",
		"ROW   |275               |8         |-557 |RDAP - Expecting HTTP status code 200 but got 508                                                       |0   |71       |",
		"ROW   |276               |8         |-558 |RDAP - Expecting HTTP status code 200 but got 510                                                       |0   |72       |",
		"ROW   |277               |8         |-559 |RDAP - Expecting HTTP status code 200 but got 511                                                       |0   |73       |",
		"ROW   |278               |8         |-560 |RDAP - Expecting HTTP status code 200 but got unexpected status code                                    |0   |74       |",
		"ROW   |279               |8         |-415 |RDAP - Maximum HTTP redirects were hit while trying to connect to RDAP server                           |0   |16       |",
		"ROW   |280               |8         |-2   |RDAP - IP addresses for the hostname are not supported by the IP versions supported by the probe node   |0   |1        |",
		"ROW   |281               |9         |-200 |IP is missing for EPP server                                                                            |0   |2        |",
		"ROW   |282               |9         |-201 |Cannot connect to EPP server                                                                            |0   |3        |",
		"ROW   |283               |9         |-202 |Invalid certificate or private key                                                                      |0   |4        |",
		"ROW   |284               |9         |-203 |First message timeout                                                                                   |0   |5        |",
		"ROW   |285               |9         |-204 |First message is invalid                                                                                |0   |6        |",
		"ROW   |286               |9         |-205 |LOGIN command timeout                                                                                   |0   |7        |",
		"ROW   |287               |9         |-206 |Invalid reply to LOGIN command                                                                          |0   |8        |",
		"ROW   |288               |9         |-207 |UPDATE command timeout                                                                                  |0   |9        |",
		"ROW   |289               |9         |-208 |Invalid reply to UPDATE command                                                                         |0   |10       |",
		"ROW   |290               |9         |-209 |INFO command timeout                                                                                    |0   |11       |",
		"ROW   |291               |9         |-210 |Invalid reply to INFO command                                                                           |0   |12       |",
		"ROW   |292               |9         |-1   |Internal error                                                                                          |0   |0        |",
		"ROW   |293               |9         |-211 |Server certificate validation failed                                                                    |0   |13       |",
		"ROW   |294               |9         |-2   |EPP - IP addresses for the hostname are not supported by the IP versions supported by the probe node    |0   |1        |",
		NULL
	};

	int			i;

	for (i = 0; NULL != data[i]; i++)
	{
		zbx_uint64_t	valuemap_mappingid, sortorder;

		if (0 == strncmp(data[i], "--", ZBX_CONST_STRLEN("--")))
			continue;

		if (2 != sscanf(data[i], "ROW |" ZBX_FS_UI64 " |%*m[^|] |%*m[^|] |%*m[^|] |0 |" ZBX_FS_UI64 "|",
				&valuemap_mappingid, &sortorder))
		{
			zabbix_log(LOG_LEVEL_CRIT, "failed to parse the following line:\n%s", data[i]);
			return FAIL;
		}

		if (ZBX_DB_OK > DBexecute(
				"update valuemap_mapping"
				" set sortorder=" ZBX_FS_UI64
				" where valuemap_mappingid=" ZBX_FS_UI64,
				sortorder, valuemap_mappingid))
		{
			return FAIL;
		}
	}

	return SUCCEED;
}

/* 6000004, 6 - add 3 new simple check (RTT) errors to value maps */
static int	DBpatch_6000004_6(void)
{
	ONLY_SERVER();

	/* this is just a direct paste from data.tmpl, with each line quoted and properly indented */
	static const char	*const data[] = {
		"ROW   |301               |7         |-226 |No IP Addresses found when resolving hostname                                                           |0   |15       |",
		"ROW   |302               |7         |-254 |No IP Addresses found when resolving hostname                                                           |0   |22       |",
		"ROW   |303               |8         |-404 |No IP Addresses found when resolving hostname                                                           |0   |8        |",
		NULL
	};

	int			i;

	for (i = 0; NULL != data[i]; i++)
	{
		zbx_uint64_t	valuemap_mappingid, valuemapid, sortorder;
		char		*value = NULL, *newvalue = NULL, *value_esc, *newvalue_esc;

		if (0 == strncmp(data[i], "--", ZBX_CONST_STRLEN("--")))
			continue;

		if (5 != sscanf(data[i], "ROW |" ZBX_FS_UI64 " |" ZBX_FS_UI64 " |%m[^|]|%m[^|]|0 |" ZBX_FS_UI64 "|",
				&valuemap_mappingid, &valuemapid, &value, &newvalue, &sortorder))
		{
			zabbix_log(LOG_LEVEL_CRIT, "failed to parse the following line:\n%s", data[i]);
			zbx_free(value);
			zbx_free(newvalue);
			return FAIL;
		}

		zbx_rtrim(value, ZBX_WHITESPACE);
		zbx_rtrim(newvalue, ZBX_WHITESPACE);

		/* NOTE: to keep it simple assume that data does not contain sequences "&pipe;", "&eol;" or "&bsn;" */

		value_esc = zbx_db_dyn_escape_string(value);
		newvalue_esc = zbx_db_dyn_escape_string(newvalue);
		zbx_free(value);
		zbx_free(newvalue);

		if (ZBX_DB_OK > DBexecute("insert into valuemap_mapping (valuemap_mappingid,valuemapid,value,newvalue,sortorder)"
				" values (" ZBX_FS_UI64 "," ZBX_FS_UI64 ",'%s','%s'," ZBX_FS_UI64 ")",
				valuemap_mappingid, valuemapid, value_esc, newvalue_esc, sortorder))
		{
			zbx_free(value_esc);
			zbx_free(newvalue_esc);
			return FAIL;
		}

		zbx_free(value_esc);
		zbx_free(newvalue_esc);
	}

	return SUCCEED;
}

DBPATCH_START(6000)

/* version, duplicates flag, mandatory flag */

DBPATCH_ADD(6000000, 0, 1)
DBPATCH_RSM(6000000, 1, 0, 0)	/* create {$RSM.PROXY.IP}, {$RSM.PROXY.PORT} macros */
DBPATCH_RSM(6000000, 2, 0, 1)	/* create provisioning_api_log table */
DBPATCH_RSM(6000000, 3, 0, 0)	/* remove {$RSM.EPP.ENABLED} and {$RSM.TLD.EPP.ENABLED} macros from rsm.dns[] and rsm.rdds[] items */
DBPATCH_RSM(6000000, 4, 0, 0)	/* rename {$RSM.TLD.RDDS.43.SERVERS} to {$RSM.TLD.RDDS43.SERVER} */
DBPATCH_RSM(6000000, 5, 0, 0)	/* rename {$RSM.TLD.RDDS.80.SERVERS} to {$RSM.TLD.RDDS80.URL} */
DBPATCH_RSM(6000000, 6, 0, 0)	/* update rsm.rdds[] items to use {$RSM.TLD.RDDS43.SERVER} and {$RSM.TLD.RDDS80.URL} */
DBPATCH_RSM(6000000, 7, 0, 0)	/* split {$RSM.TLD.RDDS.ENABLED} macro into {$RSM.TLD.RDDS43.ENABLED} and {$RSM.TLD.RDDS80.ENABLED} */
DBPATCH_RSM(6000000, 8, 0, 0)	/* replace {$RSM.TLD.RDDS.ENABLED} macro with {$RSM.TLD.RDDS43.ENABLED} and {$RSM.TLD.RDDS80.ENABLED} in rsm.dns[] and rsm.rdds[] item keys */
DBPATCH_RSM(6000000, 9, 0, 0)	/* split rdds.enabled item into rdds43.enabled and rdds80.enabled */
DBPATCH_RSM(6000000, 10, 0, 0)	/* replace obsoleted positional macros $1 and $2 in item names */
DBPATCH_RSM(6000000, 11, 0, 0)	/* register Provisioning API module and create its users */
DBPATCH_RSM(6000000, 12, 0, 0)	/* create a template for storing value maps */
DBPATCH_RSM(6000000, 13, 0, 0)	/* enable show_technical_errors */
DBPATCH_RSM(6000000, 14, 0, 0)	/* reset items.lifetime and items.request_method to default values */
DBPATCH_RSM(6000000, 15, 0, 0)	/* add missing macros - {$RDAP.BASE.URL}, {$RDAP.TEST.DOMAIN}, {$RSM.RDDS43.TEST.DOMAIN} */
DBPATCH_RSM(6000000, 16, 0, 1)	/* create table rsm_false_positive, this is required by frontend */
DBPATCH_RSM(6000000, 17, 0, 0)	/* add userid index to table rsm_false_positive */
DBPATCH_RSM(6000000, 18, 0, 0)	/* add userid foreign key to table rsm_false_positive */
DBPATCH_RSM(6000000, 19, 0, 0)	/* add eventid index to table rsm_false_positive */
DBPATCH_RSM(6000000, 20, 0, 0)	/* add eventid foreign key to table rsm_false_positive */
DBPATCH_RSM(6000000, 21, 0, 0)	/* drop column events.false_positive */
DBPATCH_RSM(6000000, 22, 0, 1)  /* add settings for "Read-only user" and "Power user" roles */
DBPATCH_RSM(6000000, 23, 0, 0)  /* add script "DNSViz webhook" */
DBPATCH_RSM(6000000, 24, 0, 0)  /* add action "Create DNSViz report" for DNSSEC accidents */
DBPATCH_RSM(6000000, 25, 0, 0)  /* create host group "Value Maps" */
DBPATCH_RSM(6000000, 26, 0, 0)  /* add "Value Maps" host group to "Template Value Maps" */
DBPATCH_RSM(6000000, 27, 0, 0)  /* allow "Power user" and "Read-only user" user groups to access "Value Maps" host group */
DBPATCH_ADD(6000001, 0, 0)
DBPATCH_ADD(6000002, 0, 0)
DBPATCH_ADD(6000003, 0, 0)
DBPATCH_ADD(6000004, 0, 0)
DBPATCH_RSM(6000004, 1, 0, 1)  /* add macro {$RDAP.TEST.DOMAIN.DYNAMIC}, {$RDAP.RDAPOUTPUT.DIRECTORY} as item parameters and fix some macro descriptions */
DBPATCH_RSM(6000004, 2, 0, 0)  /* add macro {$RDAP.TEST.DOMAIN.DYNAMIC} to each Rsmhost template */
DBPATCH_RSM(6000004, 3, 0, 0)  /* change DNSViz action from Webhook to Script */
DBPATCH_RSM(6000004, 4, 0, 0)  /* update service triggers to have manual close */
DBPATCH_RSM(6000004, 5, 0, 0)  /* fix sort order of the value map entries */
DBPATCH_RSM(6000004, 6, 0, 0)  /* add 3 new simple check (RTT) errors to value maps */

DBPATCH_END()

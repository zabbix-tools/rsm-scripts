#!/usr/bin/env perl

use FindBin;
use lib "$FindBin::RealBin";
use lib "$FindBin::RealBin/..";

use strict;
use warnings;

use Time::Piece;
use JSON::XS;
use Data::Dumper;
use Time::Local;

use RSM;
use RSMSLV;
use TLD_constants qw(:general :api);
use TLDs;

use constant DELAY         => 300;
use constant SEC_PER_DAY   => 86400;

use constant DYNAMIC_MACRO => '{$RDAP.TEST.DOMAIN.DYNAMIC}';

use constant DATE_FMT      => '%Y-%m-%d';

$Data::Dumper::Terse  = 1;    # do not output names like "$VAR1 = "
$Data::Dumper::Pair   = ": "; # use separator instead of " => "
$Data::Dumper::Useqq  = 1;    # use double quotes instead of single quotes
$Data::Dumper::Indent = 1;    # 1 provides less indentation instead of 2

sub main()
{
	parse_opts();
	setopt('nolog');

	my $current_time  = time();
	my $current_cycle = $current_time - ($current_time % DELAY);

	my $config = get_rsm_config();

	validate_config($config);

	my $cycles = get_cycles($config, $current_time);

	foreach my $date (sort(keys(%{$cycles})))
	{
		foreach (sort(keys(%{$cycles->{$date}})))
		{
			if (opt('debug'))
			{
				print(scalar(localtime($_)));
				print(" <---") if ($_ == $current_cycle);
				print("\n");
			}
		}
	}

	my $data_dir = $config->{'dynamic_rdap'}{'data_dir'};

	set_slv_config($config);

	my $server_key = get_rsm_local_key($config);
	my $section = $config->{$server_key};

	db_connect($server_key);

	foreach my $rsmhost (@{get_tlds()})
	{
		set_log_tld($rsmhost);
		update_rsmhost($data_dir, get_hostmacroids(), $rsmhost, $cycles);
		unset_log_tld();
	}

	if (opt('debug'))
	{
		my $rows = db_select(
			"select h.host,m.value".
			" from hosts h,hostmacro m".
			" where h.hostid=m.hostid".
				" and m.macro=?", [DYNAMIC_MACRO]);

		foreach my $row (@{$rows})
		{
			my $host  = $row->[0];
			my $value = $row->[1];

			my @pairs = split(',', $value);
			my $info = "";
			foreach my $pair (@pairs)
			{
				my ($ts, $domain) = split('=', $pair);
				my ($sec, $min, $hour) = localtime($ts);

				$info .= sprintf("%02d:%02d:%02d=%s ", $hour, $min, $sec, $domain);
			}

			dbg(sprintf("%30s | %s", $host, $info));
		}
	}

	db_disconnect();
}

sub validate_config($)
{
	my $config = shift;

	if (!exists($config->{'dynamic_rdap'}{'data_dir'}))
	{
		fail("\"data_dir\" must be defined in \"dynamic_rdap\" section of RSM configuration file");
	}

	my $data_dir = $config->{'dynamic_rdap'}{'data_dir'};
	if (! -d $data_dir)
	{
		fail("directory \"$data_dir\" defined in RSM configuration file does not exist");
	}

	if (!exists($config->{'dynamic_rdap'}{'past_cycles'}))
	{
		fail("\"past_cycles\" must be defined in \"dynamic_rdap\" section of RSM configuration file");
	}

	if (!exists($config->{'dynamic_rdap'}{'future_cycles'}))
	{
		fail("\"future_cycles\" must be defined in \"dynamic_rdap\" section of RSM configuration file");
	}
}

sub get_hostmacroids()
{
	my $rows = db_select(
		"select h.host,m.hostmacroid,m.value".
		" from hosts h,hostmacro m".
		" where h.hostid=m.hostid".
			" and m.macro=?",
		[DYNAMIC_MACRO]);

	my %hostmacroids;

	map {$hostmacroids{$_->[0]} = {'hostmacroid' => $_->[1], 'value' => $_->[2]}} (@{$rows});

	return \%hostmacroids;
}

sub get_json($$)
{
	my $file = shift;
	my $json = shift; # reference

	if (! -r $file)
	{
		dbg("warning, file \"$file\" not found, skipping Rsmhost...");
		return;
	}

	dbg("reading file $file...");

	my ($buf, $error);
	if (read_file($file, \$buf, \$error) != SUCCESS)
	{
		wrn("cannot read \"$file\": $error");
		return;
	}

	$$json = decode_json($buf);

	# success
	return 1;
}

# return required cycles in format
# {
#     DATE1 => {
#         TIME1 => undef,
#         TIME2 => undef,
#         ...
#     },
#     DATE2 => {
#        ...
#     }
# }
sub get_cycles($$)
{
	my $config       = shift;
	my $current_time = shift;

	my $current_cycle = $current_time - ($current_time % DELAY);

	my (%cycles, $time, $date);

	my $past_cycles = $config->{'dynamic_rdap'}{'past_cycles'};

	while ($past_cycles-- > 0)
	{
		$time = $current_cycle - DELAY - (DELAY * $past_cycles);
		$date = localtime($time)->strftime(DATE_FMT);

		$cycles{$date}{$time} = undef;
	}

	$date = localtime($current_cycle)->strftime(DATE_FMT);
	$cycles{$date}{$current_cycle} = undef;

	my $future_cycles = 0;

	while ($future_cycles++ < $config->{'dynamic_rdap'}{'future_cycles'})
	{
		$time = $current_cycle + (DELAY * $future_cycles);
		$date = localtime($time)->strftime(DATE_FMT);

		$cycles{$date}{$time} = undef;
	}

	return \%cycles;
}

# generate the following data structure from input json:
# {"<city>": {<clock>: "<domain>", ...}}
# this includes "default" city and all the cities available in json file
sub get_input_domains($$$$)
{
	my $json          = shift;
	my $cycles        = shift;
	my $date_clock    = shift;
	my $input_domains = shift;

	#info("$date_clock need to find ", join(",", map {scalar(localtime($_))} sort(keys(%{$cycles}))));

	# keep track of how many collected to stop when we're done
	my %collected = ('default' => 0, 'probes' => {});

	foreach my $city (sort(keys(%{$json->{'domains'}})))
	{
		foreach my $order_object (@{$json->{'domains'}{$city}})
		{
			my $clock = $date_clock + DELAY * ($order_object->{'order'} - 1);

			if (exists($cycles->{$clock}))
			{
				if ($city eq 'default')
				{
					$input_domains->{'default'}{$clock} = $order_object->{'domain'};

					$collected{'default'}++;

					last if ($collected{'default'} == scalar(keys(%{$cycles})));
				}
				else
				{
					$input_domains->{'cities'}{$city}{$clock} = $order_object->{'domain'};

					$collected{'probes'}{$city} = (exists($collected{'probes'}{$city}) ? $collected{'probes'}{$city} + 1 : 1);

					last if ($collected{'probes'}{$city} == scalar(keys(%{$cycles})));
				}

			}
		}
	}

	if (!exists($input_domains->{'default'}))
	{
		return;
	}

	# success
	return 1;
}

sub get_output_domains($$)
{
	my $input_domains = shift;
	my $probes        = shift;

	my %output_domains;

	foreach my $probe (keys(%{$probes}))
	{
		# in the input file city names do not contain "-ServerN" suffix
		my $city = (split("-", $probe))[0];

		if (exists($input_domains->{'cities'}{$city}))
		{
			$output_domains{'probes'}{$probe} = $input_domains->{'cities'}{$city};
		}
	}

	return \%output_domains;
}

sub construct_macro_value($)
{
	my $hash = shift;

	my @entries;
	foreach my $clock (sort(keys(%{$hash})))
	{
		push(@entries, "$clock=$hash->{$clock}");
	}

	fail("unexpected clock-domain hash: ", Dumper($hash)) unless (scalar(@entries));

	return join(',', @entries);
}

my %hostids;
sub get_probe_hostid($$)
{
	my $rsmhost = shift;
	my $probe   = shift;

	if (!$hostids{$rsmhost}{"$rsmhost $probe"})
	{
		my $rows = db_select("select host,hostid from hosts where host like ?", ["$rsmhost %"]);

		map {$hostids{$rsmhost}{$_->[0]} = $_->[1]} (@{$rows});
	}

	return $hostids{$rsmhost}{"$rsmhost $probe"};
}

sub update_rsmhost($$$$)
{
	my $data_dir     = shift;
	my $hostmacroids = shift;
	my $rsmhost      = shift;
	my $cycles       = shift;

	my $input_domains = {};

	foreach my $date (sort(keys(%{$cycles})))
	{
		my $file = sprintf("%s/%s-%s.json", $data_dir, $rsmhost, $date);

		my $json;
		return unless (get_json($file, \$json));

		my ($year, $mon, $mday) = split(/-/, $date);
		my $date_clock = timelocal(0, 0, 0, $mday, $mon-1, $year);

		if (!get_input_domains($json, $cycles->{$date}, $date_clock, $input_domains))
		{
			wrn("no \"default\" domains for current time found in the input file");
			return;
		}
	}

	my $probes = get_probes('RDAP');

	my $output_domains = get_output_domains($input_domains, $probes);

	if (opt('debug'))
	{
		dbg('default:');
		foreach my $clock (sort(keys(%{$input_domains->{'default'}})))
		{
			dbg("    ", scalar(localtime($clock)), " -- ", $input_domains->{'default'}{$clock});
		}
	}

	my $cycles_count = 0;
	foreach my $clocks (values(%{$cycles}))
 	{
		$cycles_count++ foreach (keys(%{$clocks}));
	}

	my $new_value;

	$new_value = construct_macro_value($input_domains->{'default'});

	# update template macro
	db_exec("update hostmacro set value=? where hostmacroid=?",
		[
			$new_value,
			$hostmacroids->{"Template Rsmhost Config $rsmhost"}{'hostmacroid'}
		]) unless ($new_value eq $hostmacroids->{"Template Rsmhost Config $rsmhost"}{'value'});

	# delete probe macros
	foreach my $probe (keys(%{$probes}))
	{
		if (!exists($output_domains->{'probes'}{$probe}) && exists($hostmacroids->{"$rsmhost $probe"}))
		{
			db_exec("delete from hostmacro where hostmacroid=?",
				[$hostmacroids->{"$rsmhost $probe"}{'hostmacroid'}]);
			next;
		}
	}

	# update probe macros
	foreach my $probe (keys(%{$output_domains->{'probes'}}))
	{
		if (scalar(keys(%{$output_domains->{'probes'}{$probe}})) != $cycles_count)
		{
			dbg(sprintf("missing some of the \"%s\" domains (expected %d, got %d)",
				$probe, $cycles_count, scalar(keys(%{$output_domains->{'probes'}{$probe}}))));

			# find out if there is a default for a missing Probe cycle
			foreach my $ts (keys(%{$input_domains->{'default'}}))
			{
				if (!exists($output_domains->{'probes'}{$probe}{$ts}))
				{
					$output_domains->{'probes'}{$probe}{$ts} = $input_domains->{'default'}{$ts};
				}
			}
		}

		$new_value = construct_macro_value($output_domains->{'probes'}{$probe});

		if (exists($hostmacroids->{"$rsmhost $probe"}))
		{
			db_exec("update hostmacro set value=? where hostmacroid=?",
				[
					$new_value,
					$hostmacroids->{"$rsmhost $probe"}{'hostmacroid'}
				]) unless ($new_value eq $hostmacroids->{"$rsmhost $probe"}{'value'});
		}
		else
		{
			my $macroid = get_nextid();

			db_exec("insert into hostmacro set hostmacroid=?,hostid=?,macro=?,value=?,description=?",
				[
					$macroid,
					get_probe_hostid($rsmhost, $probe),
					DYNAMIC_MACRO,
					$new_value,
					''
				]);
		}

		if (opt('debug'))
		{
			dbg("$probe:");
			foreach my $clock (sort(keys(%{$output_domains->{'probes'}{$probe}})))
			{
				dbg("    ", scalar(localtime($clock)), " -- ", $output_domains->{'probes'}{$probe}{$clock});
			}
		}
	}
}

sub get_nextid()
{
	my $rows = db_select("select nextid from ids where table_name=? and field_name=?", ["hostmacro", "hostmacroid"]);

	my $nextid;
	if (scalar(@{$rows}) != 0)
	{
		$nextid = $rows->[0][0];
	}
	else
	{
		$nextid = db_select_value("select max(hostmacroid) from hostmacro");
	}

	$nextid++;

	db_exec("update ids set nextid=? where table_name=? and field_name=?", [$nextid, "hostmacro", "hostmacroid"]);

	return $nextid;
}

main();


__END__

=head1 NAME

update-macros.pl - update dynamic RDAP domain name macros

=head1 SYNOPSIS

update-macros.pl [--dry-run] [--debug] [--help]

=head1 OPTIONS

=over 8

=item B<--dry-run>

Print data to the screen, do not change anything in the system.

=item B<--debug>

Run the script in debug mode. This means printing more information.

=item B<--help>

Print a brief help message and exit.

=back

=head1 DESCRIPTION

B<This program> will update macros holding dynamic domain names of all the Rsmhosts on all available servers.

=head1 EXAMPLES

./update-macros.pl

=cut

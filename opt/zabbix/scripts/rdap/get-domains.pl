#!/usr/bin/env perl

use FindBin;
use lib "$FindBin::RealBin";
use lib "$FindBin::RealBin/..";

use strict;
use warnings;

use Time::Piece;
use JSON::XS;
use Data::Dumper;
use Time::Local;
use MIME::Base64;

use RSM;
use RSMSLV;
use TLD_constants qw(:general :api);
use TLDs;

use constant DELAY                 => 300;
use constant SEC_PER_DAY           => 86400;
use constant DATE_FMT              => '%Y-%m-%d';
use constant DATE_FMT_REGEX        => '^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$';

use constant HTTP_STATUS_OK        => 200;
use constant HTTP_STATUS_NOT_FOUND => 404;

$Data::Dumper::Terse  = 1;    # do not output names like "$VAR1 = "
$Data::Dumper::Pair   = ": "; # use separator instead of " => "
$Data::Dumper::Useqq  = 1;    # use double quotes instead of single quotes
$Data::Dumper::Indent = 1;    # 1 provides less indentation instead of 2

sub main()
{
	parse_opts('date=s');
	setopt('nolog');

	my $current_time  = time();
	my $current_cycle = $current_time - ($current_time % DELAY);

	my ($timestamp, $date, @rsmhosts_incomplete) = get_dates();

	my $config = get_rsm_config();

	validate_config($config);

	my $data_dir = $config->{'dynamic_rdap'}{'data_dir'};

	set_slv_config($config);

	my $server_key = get_rsm_local_key($config);
	my $section = $config->{$server_key};

	db_connect($server_key);

	my $rsmhosts = get_tlds();

	# in response from TestDomainApi the city names do not contain "-ServerN" suffix
	my %probes = map {(split('-', $_))[0] => undef} (keys(%{get_probes()}));

	foreach my $rsmhost (@{$rsmhosts})
	{
		my $error;

		set_log_tld($rsmhost);

		if (tld_service_enabled($rsmhost, 'rdap', $current_cycle))
		{
			my $url = get_url($config->{'dynamic_rdap'}{'base_url'}, $rsmhost, $date);

			my $body = http_request(
					$rsmhost,
					$url,
					$config->{'dynamic_rdap'}{'username'},
					$config->{'dynamic_rdap'}{'password'},
					$config->{'dynamic_rdap'}{'timeout'},
					$config->{'dynamic_rdap'}{'retries'},
				);

			if (!$body || validate_body($body, \%probes) != SUCCESS)
			{
				push(@rsmhosts_incomplete, $rsmhost);
			}

			if ($body)
			{
				my $file = get_file_name($data_dir, $rsmhost, $date);

				dbg("writing body to $file...");

				if (write_file($file, $body, \$error) != SUCCESS)
				{
					fail($error);
				}
			}
		}

		unset_log_tld();
	}

	db_disconnect();

	if (scalar(@rsmhosts_incomplete) != 0)
	{
		wrn("requests for the following Rsmhosts containted incomplete/unexpected data: ", join(',', @rsmhosts_incomplete));
	}
}

sub validate_config($)
{
	my $config = shift;

	if (!exists($config->{'dynamic_rdap'}{'data_dir'}))
	{
		fail("\"data_dir\" must be defined in \"dynamic_rdap\" section of RSM configuration file");
	}

	if (! -d $config->{'dynamic_rdap'}{'data_dir'})
	{
		fail("directory \"$config->{'dynamic_rdap'}{'data_dir'}\" defined in \"dynamic_rdap\" section".
				" of RSM configuration file does not exist");
	}

	if (!exists($config->{'dynamic_rdap'}{'base_url'}))
	{
		fail("\"base_url\" must be defined in \"dynamic_rdap\" section of RSM configuration file");
	}

	if (!exists($config->{'dynamic_rdap'}{'username'}))
	{
		fail("\"username\" must be defined in \"dynamic_rdap\" section of RSM configuration file");
	}

	if (!exists($config->{'dynamic_rdap'}{'password'}))
	{
		fail("\"password\" must be defined in \"dynamic_rdap\" section of RSM configuration file");
	}

	if (!exists($config->{'dynamic_rdap'}{'timeout'}))
	{
		fail("\"timeout\" must be defined in \"dynamic_rdap\" section of RSM configuration file");
	}

	if (!exists($config->{'dynamic_rdap'}{'retries'}))
	{
		fail("\"retries\" must be defined in \"dynamic_rdap\" section of RSM configuration file");
	}
}

sub get_dates()
{
	my $current_time = time();

	my $date;
	if (opt('date'))
	{
		$date = getopt('date');

		fail("$date: invalid date, expected YYYY-MM-DD") unless ($date =~ DATE_FMT_REGEX);
	}
	else
	{
		$date = (localtime($current_time + SEC_PER_DAY))->strftime(DATE_FMT);
	}

	my ($year, $mon, $mday) = split(/[\s.-]+/, $date);
	my $timestamp = timelocal(0, 0, 0, $mday, $mon - 1, $year);

	if ($timestamp > $current_time)
	{
		$timestamp = $current_time;
	}

	return $timestamp, $date;
}

sub get_file_name($$$)
{
	my $data_dir = shift;
	my $rsmhost  = shift;
	my $date     = shift;

	return $data_dir . '/' . $rsmhost . '-' . $date . '.json';
}

sub http_request($$$$$$)
{
	my $rsmhost  = shift;
	my $url      = shift;
	my $username = shift;
	my $password = shift;
	my $timeout  = shift;
	my $retries  = shift;

	dbg("sending request (url:$url, timeout:$timeout, retries:$retries)");

	my $method = 'GET';

	my $user_agent = LWP::UserAgent->new('timeout' => $timeout);
	my $request = HTTP::Request->new($method, $url, undef, undef);

	$request->header('Authorization' => 'Basic ' . encode_base64($username . ':' . $password));

	my $response;
	my $attempt = 0;

	while ($attempt <= $retries)
	{
		$attempt++;
		dbg("sending request (attempt $attempt)...") if ($retries && $attempt != 1);
		$response = $user_agent->simple_request($request);
		dbg("  received HTTP status code: " . $response->code()) if ($retries);

		last if ($response->code() == HTTP_STATUS_OK);
	}

	my $code = $response->code();
	my $type = $response->header('content-type');
	my $body = $response->content();

	if ($code == HTTP_STATUS_NOT_FOUND)
	{
		dbg('Rsmhost not found: "' . ($rsmhost // 'undef') . '"');
		return;
	}

	dbg('received (status code: ' . $code . '):');
	dbg($_) foreach (split(/\n/, $body));

	http_validate_response($rsmhost, $code, $type);

	#$json = JSON::XS->new()->boolean_values(0, 1)->decode($body);

	return $body;
}

sub http_validate_response($$$)
{
	my $rsmhost      = shift;
	my $status_code  = shift;
	my $content_type = shift;

	if ($status_code != HTTP_STATUS_OK)
	{
		pfail('unexpected status code: "' . ($status_code // 'undef') . '"');
	}
	if (!defined($content_type) || $content_type ne 'application/json')
	{
		pfail('unexpected content type: "' . ($content_type // 'undef') . '"');
	}
}

sub get_url($$$)
{
	my $base_url = shift;
	my $rsmhost  = shift;
	my $date     = shift; # YYYY-MM-DD

	return $base_url . '/' . $rsmhost . '/' . $date;
}

sub validate_body($$)
{
	my $body   = shift;
	my $probes = shift;

	my $hash;
	eval
	{
		$hash = decode_json($body);
		1;
	}
	or do
	{
		db_disconnect();

		my $e = $@;
		pfail("response contains invalid JSON: ", $e =~ s/(\r?\n)+$//r);
	};

	if (!exists($hash->{"domains"}{"default"}))
	{
		dbg("\"default\" section was not found in the body");

		return E_FAIL;
	}

	foreach my $probe_or_default (sort(keys(%{$hash->{'domains'}})))
	{
		if (($probe_or_default ne "default") && !exists($probes->{$probe_or_default}))
		{
			dbg("unknown Probe \"$probe_or_default\" was found in response");

			return E_FAIL;
		}

		if (scalar(@{$hash->{"domains"}{$probe_or_default}}) < 288)
		{
			dbg("\"$probe_or_default\" contains only ", scalar(@{$hash->{"domains"}{$probe_or_default}}),
					" domains, while it's expected to have 288");

			return E_FAIL;
		}

		if (scalar(@{$hash->{"domains"}{$probe_or_default}}) > 288)
		{
			dbg("\"$probe_or_default\" contains ", scalar(@{$hash->{"domains"}{$probe_or_default}}),
					" domains, while it's expected to have exactly 288");

			return E_FAIL;
		}
	}

	return SUCCESS;
}

main();


__END__

=head1 NAME

get-domains.pl - download dynamic RDAP domains for all Rsmhosts

=head1 SYNOPSIS

get-domains.pl [--dry-run] [--debug] [--help]

=head1 OPTIONS

=over 8

=item B<--dry-run>

Print data to the screen, do not change anything in the system.

=item B<--debug>

Run the script in debug mode. This means printing more information.

=item B<--help>

Print a brief help message and exit.

=back

=head1 DESCRIPTION

B<This program> will download dynamic RDAP domain names of all the Rsmhosts on all available servers.

=head1 EXAMPLES

./get-domains.pl

=cut

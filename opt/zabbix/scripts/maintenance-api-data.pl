#!/usr/bin/env perl

use FindBin;
use lib $FindBin::RealBin;

use strict;
use warnings;

use ApiHelper;
use Getopt::Long;
use Pod::Usage;
use Path::Tiny qw(path);
use TLD_constants qw(:api);
use RSM;
use RSMSLV; # required for ApiHelper

use constant JSON_OBJECT_DISABLED_SERVICE => {
	'status' => JSON_VALUE_DISABLED_SERVICE
};

parse_opts('ignore-file=s');

setopt('nolog');

my %ignore;

if (opt('ignore-file'))
{
	my ($buf, $error);

	if (! -f getopt('ignore-file'))
	{
		fail(getopt('ignore-file') . ": this file does not exist or is not a file");
	}

	if (read_file(getopt('ignore-file'), \$buf, \$error) != SUCCESS)
	{
		fail("error reading \"" . getopt('ignore-file') . "\": $error");
	}

	map {$ignore{$_} = 1;} (split('\n', $buf));
}

my $error = rsm_targets_prepare(AH_SLA_API_TMP_DIR, AH_SLA_API_DIR);

fail($error) if ($error);

my $now = time();

set_slv_config(get_rsm_config());

foreach my $version (@{+AH_SLA_API_VERSIONS})
{
	foreach my $rsmhost_dir (path(AH_SLA_API_DIR . "/v$version")->children)
	{
		next unless ($rsmhost_dir->is_dir());

		my $rsmhost = $rsmhost_dir->basename();

		dbg("rsmhost=[$rsmhost]");

		next if (exists($ignore{$rsmhost}));

		my $json;

		print("cannot read \"$rsmhost\" state: ", ah_get_error())
			unless (ah_read_state($version, $rsmhost, \$json) == AH_SUCCESS);

		$json->{'status'} = 'Up-inconclusive';

		$json->{'testedServices'} = {
			'RDDS' => JSON_OBJECT_DISABLED_SERVICE,
		};

		db_connect();

		if (get_monitoring_target() eq MONITORING_TARGET_REGISTRY)
		{
			$json->{'testedServices'}{'DNS'}    = JSON_OBJECT_DISABLED_SERVICE;
			$json->{'testedServices'}{'DNSSEC'} = JSON_OBJECT_DISABLED_SERVICE;
			$json->{'testedServices'}{'EPP'}    = JSON_OBJECT_DISABLED_SERVICE;
		}

		if (is_rdap_standalone($now) && $version > 1)
		{
			$json->{'testedServices'}{'RDAP'}   = JSON_OBJECT_DISABLED_SERVICE;
		}

		db_disconnect();

		fail("cannot set \"$rsmhost\" state: ", ah_get_error())
			unless (ah_save_state($version, $rsmhost, $json) == AH_SUCCESS);
	}
}

$error = rsm_targets_apply();

fail($error) if ($error);

__END__

=head1 NAME

maintenance-api-data.pl - set status of all Rsmhosts in SLA API to "maintenance"

=head1 SYNOPSIS

maintenance-api-data.pl.pl [--ignore-file <file>] [--help]

=head1 OPTIONS

=over 8

=item B<--ignore-file> file

Optionally specify text file with list of new-line separated RsmHosts to ignore.

=item B<--help>

Print a brief help message and exit.

=back

=head1 DESCRIPTION

B<This program> will set status of all Rsmhosts in SLA API to "maintenance".

=head1 EXAMPLES

./maintenance-api-data.pl

=cut

#!/usr/bin/env bash

set -o pipefail
set -o nounset

LOG="/var/log/zabbix/dnsviz.log"
ERROR_PREFIX="error:"

DEFAULT_MAX_TIME=30
DEFAULT_RETRY=3
DEFAULT_RETRY_DELAY=60

usage()
{
	if [ -n "${1:-}" ]; then
		echo "${ERROR_PREFIX} $*" | tee -a ${LOG}
		echo                      | tee -a ${LOG}
	fi

	echo "usage: $0 <domain> [-v]"
	echo "e. g.: $0 www.zz--icann-monitoring.example"
	exit 1
}

get_param()
{
	sed -nr "/^\[dnsviz\]/ { :l /^$1[ ]*=/ { s/[^=]*=[ ]*//; p; q;}; n; b l;}" /opt/zabbix/scripts/rsm.conf
}

if [ -z "${1:-}" ]; then
	usage "invalid first parameter"
fi

target="$1"
VERBOSE=""

if [ -n "${2:-}" ]; then
	if [ "${2}" != "-v" ]; then
		usage "invalid second parameter"
	fi

	VERBOSE="--verbose"
fi

max_time=$(get_param max_time)
max_time=${max_time:-$DEFAULT_MAX_TIME}

retry=$(get_param retry)
retry=${retry:-$DEFAULT_RETRY}

retry_delay=$(get_param retry_delay)
retry_delay=${retry_delay:-$DEFAULT_RETRY_DELAY}

ts_start=$(date +%s)

(
	echo "---------------------------------------"
	date -d@${ts_start}
	echo "Generating DNSViz report for ${target}..."
) | tee -a ${LOG}

# original query
# curl 'https://dnsviz.net/d/www.zz--icann-monitoring.pizza/analyze/' \
#      -H 'Accept: text/html, */*; q=0.01' \
#      -H 'Accept-Language: en-GB,en;q=0.9' \
#      -H 'Cache-Control: no-cache' \
#      -H 'Connection: keep-alive' \
#      -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8' \
#      -H 'Cookie: __utma=192841471.1904863522.1652269995.1652269995.1652269995.1; __utmc=192841471; __utmz=192841471.1652269995.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); __utmt=1; __utmb=192841471.3.10.1652269995' \
#      -H 'Origin: https://dnsviz.net' \
#      -H 'Pragma: no-cache' \
#      -H 'Referer: https://dnsviz.net/d/www.zz--icann-monitoring.pizza/analyze/' \
#      -H 'Sec-Fetch-Dest: empty' \
#      -H 'Sec-Fetch-Mode: cors' \
#      -H 'Sec-Fetch-Site: same-origin' \
#      -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.127 Safari/537.36' \
#      -H 'X-Requested-With: XMLHttpRequest' \
#      -H 'sec-ch-ua: " Not A;Brand";v="99", "Chromium";v="100", "Google Chrome";v="100"' \
#      -H 'sec-ch-ua-mobile: ?0' \
#      -H 'sec-ch-ua-platform: "Linux"' \
#      --data-raw 'force_ancestor=.&ecs=&explicit_delegation=&analysis_type=0&perspective=server&sockname=' \
#      --compressed

# --data-raw   | force ancestor to root (.)
# --compressed | compress request and decompress response
# --location   | follow redirects
# --silent     | do not print progress bar
# --write-out  | print resulting HTTP status code

curl https://dnsviz.net/d/${target}/analyze/                                                              \
     -H 'Cache-Control: no-cache'                                                                         \
     -H 'Connection: keep-alive'                                                                          \
     -H 'Content-Type: application/x-www-form-urlencoded; charset=UTF-8'                                  \
     -H 'Pragma: no-cache'                                                                                \
     --data-raw 'force_ancestor=.&ecs=&explicit_delegation=&analysis_type=0&perspective=server&sockname=' \
     --compressed                                                                                         \
     --location                                                                                           \
     --silent                                                                                             \
     --fail                                                                                               \
     --max-time ${max_time}                                                                               \
     --retry ${retry}                                                                                     \
     --retry-delay ${retry_delay}                                                                         \
     ${VERBOSE}                                                                                           \
     --write-out "%{http_code}\n" 2>&1 | tee -a ${LOG}

rv=$?

ts_end=$(date +%s)
took=$(echo $ts_end-$ts_start | bc)

if [ $rv -ne 0 ]; then
	echo "${ERROR_PREFIX} curl failed with error code $rv (took ${took} seconds)" | tee -a ${LOG}
	exit $rv
fi

echo "Success (took ${took} seconds)"'!' | tee -a ${LOG}

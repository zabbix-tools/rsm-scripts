<?php
/*
** Zabbix
** Copyright (C) 2001-2019 Zabbix SIA
**
** This program is free software; you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation; either version 2 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
**/


define('USER_TYPE_READ_ONLY',		100); /* keep the same ID as the role */
define('USER_TYPE_POWER_USER',		110); /* keep the same ID as the role */
define('USER_TYPE_COMPLIANCE',		120);

/*
 * These numbers must be in sync with Backend!
 */

// SLA monitoring start year.
define('SLA_MONITORING_START_YEAR',	2014);

// SLA monitoring extra filter value.
define('SLA_MONITORING_SLV_FILTER_NON_ZERO',	-1);
define('SLA_MONITORING_SLV_FILTER_ANY',			-2);

// SLA monitoring services.
define('RSM_DNS',			0);
define('RSM_DNSSEC',		1);
define('RSM_RDDS',			2);
define('RSM_EPP',			3);
define('RSM_RDAP',			4); // Standalone RDAP

// SLA monitoring macros.
define('RSM_PAGE_SLV',				'{$RSM.ROLLWEEK.THRESHOLDS}');
define('RSM_ROLLWEEK_SECONDS',		'{$RSM.ROLLWEEK.SECONDS}');
define('RSM_RDDS_ENABLED',			'{$RSM.RDDS.ENABLED}');
define('RSM_TLD_DNSSEC_ENABLED',	'{$RSM.TLD.DNSSEC.ENABLED}');
define('RSM_TLD_EPP_ENABLED',		'{$RSM.TLD.EPP.ENABLED}');
define('RSM_TLD_RDDS43_ENABLED',	'{$RSM.TLD.RDDS43.ENABLED}');
define('RSM_TLD_RDDS80_ENABLED',	'{$RSM.TLD.RDDS80.ENABLED}');
define('RSM_PROBE_AVAIL_LIMIT',		'{$RSM.PROBE.AVAIL.LIMIT}');
define('RSM_RDAP_TLD_ENABLED',		'{$RDAP.TLD.ENABLED}');
define('RDAP_BASE_URL',				'{$RDAP.BASE.URL}');
define('RDDS43_ENABLED',			'rdds43.enabled');
define('RDDS80_ENABLED',			'rdds80.enabled');
define('RDAP_ENABLED',				'rdap.enabled');

// Wow often do we save history of Probe ONLINE status.
define('RSM_PROBE_DELAY',			60);	// probe status delay

// SLA monitoring rolling week items keys.
define('RSM_SLV_DNS_ROLLWEEK',		'rsm.slv.dns.rollweek');
define('RSM_SLV_DNSSEC_ROLLWEEK',	'rsm.slv.dnssec.rollweek');
define('RSM_SLV_RDDS_ROLLWEEK',		'rsm.slv.rdds.rollweek');
define('RSM_SLV_RDAP_ROLLWEEK',		'rsm.slv.rdap.rollweek');
define('RSM_SLV_EPP_ROLLWEEK',		'rsm.slv.epp.rollweek');

// Template names.
define('TEMPLATE_NAME_TLD_CONFIG', 'Template Rsmhost Config %s');

// SLA monitoring availability items keys.
define('RSM_SLV_DNS_AVAIL',					'rsm.slv.dns.avail');
define('RSM_SLV_RDDS_AVAIL',				'rsm.slv.rdds.avail');
define('RSM_SLV_RDAP_AVAIL',				'rsm.slv.rdap.avail');
define('RSM_SLV_EPP_AVAIL',					'rsm.slv.epp.avail');
define('RSM_SLV_DNSSEC_AVAIL',				'rsm.slv.dnssec.avail');

// RDAP standalone.
define('RSM_RDAP_STANDALONE', '{$RSM.RDAP.STANDALONE}');

// "RSM Service Availability" value mapping:
define('DOWN',	0);	// Down

// SLA reports graph names.


// SLA monitoring incident status.
define('INCIDENT_ACTIVE',			0);
define('INCIDENT_RESOLVED',			1);
define('INCIDENT_FALSE_POSITIVE',	2);

// false positive event status.
define('INCIDENT_FLAG_NORMAL',			0);
define('INCIDENT_FLAG_FALSE_POSITIVE',	1);

// SLA monitoring incident status, specific errors: internal and DNSSEC.
define('ZBX_EC_INTERNAL_LAST',			-199);
define('ZBX_EC_DNS_UDP_DNSSEC_FIRST',	-401);	# DNS UDP - The TLD is configured as DNSSEC-enabled, but no DNSKEY was found in the apex
define('ZBX_EC_DNS_UDP_DNSSEC_LAST',	-427);	# DNS UDP - Malformed DNSSEC response
define('ZBX_EC_DNS_TCP_DNSSEC_FIRST',	-801);	# DNS TCP - The TLD is configured as DNSSEC-enabled, but no DNSKEY was found in the apex
define('ZBX_EC_DNS_TCP_DNSSEC_LAST',	-827);	# DNS TCP - Malformed DNSSEC response

// SLA monitoring calculated items keys.
define('CALCULATED_ITEM_DNS_FAIL',				'rsm.configvalue[RSM.INCIDENT.DNS.FAIL]');
define('CALCULATED_ITEM_DNSSEC_FAIL',			'rsm.configvalue[RSM.INCIDENT.DNSSEC.FAIL]');
define('CALCULATED_ITEM_RDDS_FAIL',				'rsm.configvalue[RSM.INCIDENT.RDDS.FAIL]');
define('CALCULATED_ITEM_RDAP_FAIL',				'rsm.configvalue[RSM.INCIDENT.RDAP.FAIL]');
define('CALCULATED_ITEM_EPP_FAIL',				'rsm.configvalue[RSM.INCIDENT.EPP.FAIL]');
define('CALCULATED_ITEM_DNS_DELAY',				'rsm.configvalue[RSM.DNS.DELAY]');
define('CALCULATED_ITEM_RDDS_DELAY',			'rsm.configvalue[RSM.RDDS.DELAY]');
define('CALCULATED_ITEM_RDAP_DELAY',			'rsm.configvalue[RSM.RDAP.DELAY]');
define('CALCULATED_ITEM_EPP_DELAY',				'rsm.configvalue[RSM.EPP.DELAY]');
define('CALCULATED_ITEM_DNS_UDP_RTT_HIGH',		'rsm.configvalue[RSM.DNS.UDP.RTT.HIGH]');
define('CALCULATED_ITEM_DNS_TCP_RTT_HIGH',		'rsm.configvalue[RSM.DNS.TCP.RTT.HIGH]');
define('CALCULATED_ITEM_RDDS_RTT_HIGH',			'rsm.configvalue[RSM.RDDS.RTT.HIGH]');
define('CALCULATED_ITEM_RDAP_RTT_HIGH',			'rsm.configvalue[RSM.RDAP.RTT.HIGH]');
define('CALCULATED_DNS_ROLLWEEK_SLA',			'rsm.configvalue[RSM.DNS.ROLLWEEK.SLA]');
define('CALCULATED_RDDS_ROLLWEEK_SLA',			'rsm.configvalue[RSM.RDDS.ROLLWEEK.SLA]');
define('CALCULATED_RDAP_ROLLWEEK_SLA',			'rsm.configvalue[RSM.RDAP.ROLLWEEK.SLA]');
define('CALCULATED_EPP_ROLLWEEK_SLA',			'rsm.configvalue[RSM.EPP.ROLLWEEK.SLA]');
define('CALCULATED_PROBE_RSM_IP4_ENABLED',		'probe.configvalue[RSM.IP4.ENABLED]');
define('CALCULATED_PROBE_RSM_IP6_ENABLED',		'probe.configvalue[RSM.IP6.ENABLED]');

// Number of test cycles to show before and after incident recovery event.
define('DISPLAY_CYCLES_BEFORE_RECOVERY',	4);
define('DISPLAY_CYCLES_AFTER_RECOVERY',		6);

// SLA monitoring probe status items keys.
define('PROBE_KEY_ONLINE',			'rsm.probe.online');
define('PROBE_DNS_MODE',			'rsm.dns.mode');
define('PROBE_DNS_NS_STATUS',		'rsm.dns.ns.status');
define('PROBE_DNS_NSID',			'rsm.dns.nsid[{#NS},{#IP}]');
define('PROBE_DNS_RTT',				'rsm.dns.rtt');
define('PROBE_DNS_STATUS',			'rsm.dns.status');
define('PROBE_DNS_TRANSPORT',		'rsm.dns.protocol');
define('PROBE_DNSSEC_STATUS',		'rsm.dnssec.status');
define('PROBE_EPP_RESULT',			'rsm.epp[');
define('PROBE_EPP_IP',				'rsm.epp.ip[{$RSM.TLD}]');
define('PROBE_EPP_UPDATE',			'rsm.epp.rtt[{$RSM.TLD},update]');
define('PROBE_EPP_INFO',			'rsm.epp.rtt[{$RSM.TLD},info]');
define('PROBE_EPP_LOGIN',			'rsm.epp.rtt[{$RSM.TLD},login]');
define('PROBE_RDDS_STATUS',			'rsm.rdds.status');
define('PROBE_RDDS43_STATUS',		'rsm.rdds.43.status');
define('PROBE_RDDS43_IP',			'rsm.rdds.43.ip');
define('PROBE_RDDS43_RTT',			'rsm.rdds.43.rtt');
define('PROBE_RDDS43_TARGET',		'rsm.rdds.43.target');
define('PROBE_RDDS43_TESTEDNAME',	'rsm.rdds.43.testedname');
define('PROBE_RDDS80_STATUS',		'rsm.rdds.80.status');
define('PROBE_RDDS80_IP',			'rsm.rdds.80.ip');
define('PROBE_RDDS80_RTT',			'rsm.rdds.80.rtt');
define('PROBE_RDDS80_TARGET',		'rsm.rdds.80.target');
define('PROBE_RDAP_STATUS',			'rdap.status');
define('PROBE_RDAP_IP',				'rdap.ip');
define('PROBE_RDAP_RTT',			'rdap.rtt');
define('PROBE_RDAP_TARGET',			'rdap.target');
define('PROBE_RDAP_TESTEDNAME',		'rdap.testedname');

// SLA monitoring NS names.
define('NS_DOWN',		1);
define('NS_UP',			2);

// SLA monitoring probe status.
define('PROBE_OFFLINE',	-1);
define('PROBE_DOWN',	 0); // in the database, result of the test
define('PROBE_UP',		 1); // in the database, result of the test
define('PROBE_DISABLED', 2);
define('PROBE_NORESULT', 3);
define('PROBE_UNKNOWN',  4); // not disabled is the only thing known so far, temporary status

// NameServer status.
define('NAMESERVER_DOWN',	0);

// SLA monitoring "rsm" host name.
define('RSM_HOST',	'Global macro history');

// SLA monitoring TLDs group.
define('RSM_TLDS_GROUP',	'TLDs');

// TLD types.
define('RSM_CC_TLD_GROUP',		'ccTLD');
define('RSM_G_TLD_GROUP',		'gTLD');
define('RSM_OTHER_TLD_GROUP',	'otherTLD');
define('RSM_TEST_GROUP',		'testTLD');

define('RSM_RDDS_SUBSERVICE_RDDS43',	'RDDS43');
define('RSM_RDDS_SUBSERVICE_RDDS80',	'RDDS80');
define('RSM_RDDS_SUBSERVICE_RDAP',		'RDAP');

define('PROBES_MON_GROUPID',					130);

// Value maps used for special purpose.
define('RSM_VALUE_MAP_DNS_RTT',					"RSM DNS rtt");
define('RSM_VALUE_MAP_RDDS_RTT',				"RSM RDDS rtt");
define('RSM_VALUE_MAP_RDAP_RTT',				"RSM RDAP rtt");
define('RSM_VALUE_MAP_TRANSPORT_PROTOCOL',		"Transport protocol");
define('RSM_VALUE_MAP_SERVICE_AVAILABILITY',	"RSM Service Availability");

define('RSM_MONITORING_TARGET',			'{$RSM.MONITORING.TARGET}');
define('MONITORING_TARGET_REGISTRY',	'registry');
define('MONITORING_TARGET_REGISTRAR',	'registrar');

define('UP_INCONCLUSIVE_RECONFIG',	4);

define('FILTER_STATUS_ALL'                   , 0);
define('FILTER_STATUS_FAIL'                  , 1);
define('FILTER_STATUS_DISABLED'              , 2);
define('FILTER_STATUS_ALL_INCLUDING_DISABLED', 3);

// Salt used for switching frontends
// static string can be replaced with environment variable getenv('RSM_SECRET_KEY')
define('RSM_SECRET_KEY',		'An0KXLtNTwCGd2FUeKqUsJ#X0#6N%B=OVZ(sfsB&dQEx6aVte2^ZXTset&!%l4f#');
